window.$ = window.jQuery = require('jquery');

window.priceHelper =
    {
        priceWithComma: function (price) {
            var stringPrice = "" + price;
            if (stringPrice.indexOf(".") !== -1)
                stringPrice = stringPrice.replace(".", ",");

            return stringPrice;
        },

        priceWithTwoDigits: function (price) {
            var numberPrice = Number(price);
            return numberPrice.toFixed(2);
        }
    };

function priceWithComma(price) {
    var stringPrice = "" + price;
    if (stringPrice.indexOf(".") !== -1)
        stringPrice = stringPrice.replace(".", ",");

    return stringPrice;
}

function priceWithTwoDigits(price) {
    var stringPrice = "" + price;
    if (stringPrice.indexOf(".") !== -1)
        stringPrice = stringPrice.replace(".", ",");

    return stringPrice;
}
