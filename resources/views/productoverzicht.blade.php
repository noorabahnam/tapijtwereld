@extends('layouts.app')
@section('css')

    {{--here css files....--}}
@stop
@section('content')
    {!! Form::open() !!}
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-xs-12" style="background-color: #252e65; width: 100%">
                    <div class="col-xs-3 col-sm-1">
                        <a href='javascript:history.go(-1)' class="btn btn-block btn-default btn-md margin"
                           style="background-color: #252e65;"><i class="fa fa-chevron-left"
                                                                 aria-hidden="true"
                                                                 style="color: white"></i></a>
                    </div>
                    <div class="col-xs-6 col-sm-10">
                        @if($isOfferte)
                            <h6 class="col-xs-12"
                                style="color: white;  font-weight: bold; display:flex; justify-content: center; align-items:center; margin-top: 10px">
                                NIEUWE OFFERTE</h6>
                        @else
                            <h6 class="col-xs-12"
                                style="color: white; font-weight: bold; display:flex; justify-content: center; align-items:center; margin-top: 10px">
                                NIEUWE BESTELLING</h6>
                        @endif

                        <h6 class="col-xs-12"
                            style="color: #3869D4; display:flex; justify-content: center; align-items:center; margin-top: 10px">
                            Overzicht </h6>
                    </div>

                    <div class="col-xs-3 col-sm-1">
                        <a href="{{route("home")}}" class="btn btn-block btn-default btn-md margin"
                           style="background-color: #252e65"><i class="fa fa-home" aria-hidden="true"
                                                                style="color: white"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if ($errors->any())
        <div class="alert alert-danger margin">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="col-md-12" style="display:flex; justify-content: center; align-items:center; margin-top: 10px">
        <img src="{{URL::asset('/uploads/' . $company .".png")}}" alt="" style=" height: 70px"/>
    </div>


    <?php $totalPrice = 0; $totalBtw = 0; $totalInc = 0; ?>
    @foreach($products as $key => $product)

        <div class="col-md-12" id="product_{{$product->id}}">
            <div class="row margin" style="background-color: white; border-radius: 15px; color:#2d4373;">
                <table class="col-md-12">
                    <tr>
                        <td class="col-xs-2">
                            <img src="{{URL::asset('/uploads/' . $product->photo)}}" alt="" width="60"
                                 style="border-radius: 50%; margin-top: 15px;"/>
                        </td>

                        <td class="col-xs-3">
                            <h4 class="col-xs-12"
                                style="color:#2d4373;;  font-weight: bold; font-size: 22px;">{{$product->product_type}}</h4>
                            <h5 class="col-xs-12"
                                style="color: #2d4373; margin: 0; font-size: 18px; "><p>{{$product->description}}
                                </p>
                            </h5>
                            @if($product->cutMeasures != null && count(json_decode($product->cutMeasures)) > 0)
                                @foreach (json_decode($product->cutMeasures) as $arr)
                                    <h5 class="col-xs-12"
                                        style="color: #2d4373; margin: 0; font-size: 18px;">{{$arr[0] . ': ' . $arr[1] .' x '. $arr[2] .'cm '}}</h5>
                                @endforeach
                            @endif
                        </td>
                        <td class="col-xs-2">
                            <h5 class="col-xs-12"
                                style="color: #2d4373; font-size: 18px;">{{$product->count . ' ' . $product->unit}}</h5>
                        </td>
                        <td class="col-xs-3">
                            <button class="editProduct" style="justify-items: left" id="product_edit_{{$product->id}}"
                                    data-product="{{$product->id}}"><span class="glyphicon glyphicon-edit"
                                                                          style=" "></span>
                            </button>
                            <button class="copyProduct" style="justify-items: left" id="product_copy_{{$product->id}}"
                                    data-product="{{$product->id}}"><span class="glyphicon glyphicon-copy"
                                                                          style=""></span>
                            </button>
                            <button class="deleteProduct" style="justify-items: left"
                                    id="product_delete_{{$product->id}}" data-product="{{$product->id}}"><span
                                    class="glyphicon glyphicon-remove" style=" "></span>
                            </button>
                        </td>
                        <td class="col-xs-2">
                            <div class="form-group">
                                <label class="form-control"
                                       style="border-style: none; color: #2d4373; font-size: 18px;">{{App\Helpers\PriceHelper::stringPriceWithTwoDigits($product->pricePerUnit == 1 ? ($product->product_price * $product->count) : $product->product_price)}}</label>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td colspan="5">
                            <?php foreach (json_decode($product->accessories) as $arr) {
                            if (($arr[2] == 0)) continue; ?>

                            <table class="col-xs-12" style="margin-top: 5px;">
                                <tr class="col-xs-2"></tr>
                                <tr class="col-xs-3">
                                    <td class="col-xs-12"
                                        style="color: #2d4373; margin: 0; font-size: 18px;">{{$arr[0]}}</td>
                                </tr>
                                <tr class="col-xs-2">
                                    <td class="col-xs-12"
                                        style="color: #2d4373; margin: 0; font-size: 18px;"> {{App\Helpers\PriceHelper::stringPriceWithTwoDigits($arr[1]) . ' x ' . $arr[2]}}</td>
                                </tr>
                                <tr class="col-xs-3"></tr>

                                <tr class="col-xs-2">
                                    <td class="form-group">
                                        <label class="form-control"
                                               style="border-style: none; color: #2d4373; font-size: 18px;">{{App\Helpers\PriceHelper::stringPriceWithTwoDigits($arr[1] * $arr[2])}}</label>
                                    </td>
                                </tr>

                            </table>
                            <?php } ?>
                        </td>
                    </tr>
                    @if($product->plintenInfo != null && $product->plintenInfo !== "Geenplinten")
                        <tr>
                            <td class="col-xs-2"></td>

                            <td class="col-xs-3">
                                @if($product->plintenInfo != "PlintenPlak")
                                    <h5 class="col-xs-12"
                                        style="color: #2d4373; font-size: 18px;">{{$product->plintenInfo}}</h5>
                                @endif
                                <h5 class="col-xs-12"
                                    style="color: #2d4373; font-size: 18px; ">{{$product->plintenDetails}}</h5>
                                <h5 class="col-xs-12"
                                    style="color: #2d4373; font-size: 18px;">{{'€ '. $product->plintenPrice . ' per meter '}}</h5>
                            </td>

                            <td class="col-xs-2">
                                <h5 class="col-xs-12"
                                    style="color: #2d4373; font-size: 18px;">{{$product->plintCount . ' m1 '}}</h5>
                            </td>
                            <td class="col-xs-2"></td>
                            <td class="col-xs-3">
                                <div class="form-group">
                                    <label class="form-control"
                                           style="border-style: none; color: #2d4373; font-size: 18px;">{{App\Helpers\PriceHelper::stringPriceWithTwoDigits($product->plintenPrice * $product->plintCount)}}</label>
                                </div>
                            </td>
                        </tr>
                    @endif
                    <tr>
                        <td class="col-xs-2"></td>

                        <td class="col-xs-3">
                            <h5 class="col-xs-12" style="color: #2d4373; margin: 0; font-size: 18px;">Legservice</h5>
                        </td>
                        <td class="col-xs-2">
                            <h5 class="col-xs-12"
                                style="color: #2d4373; font-size: 18px;">{{$product->legService == 0 ? "Nee" : "Ja"}} </h5>
                        </td>
                        <td class="col-xs-2"></td>
                        <td class="col-xs-3">
                            <div class="form-group">
                                <label class="form-control"
                                       style="border-style: none; color: #2d4373; font-size: 18px;">{{$product->legService == 0 ? '' :App\Helpers\PriceHelper::stringPriceWithTwoDigits($product->legServicePrice)}}</label>
                            </div>
                        </td>
                    </tr>

                </table>
            </div>
        </div>

        <?php
        $totalPrice += $product->productTotalPrice;
        $totalBtw += $product->productBTW;
        $totalInc += $product->productTotalIncPrice;
        ?>
    @endforeach

    <div class="col-md-12 margin">
        <div class="form-group" style="display:flex; justify-content: center; align-items:center;">

            <a href='{{route('AddProduct')}}' class="btn btn-default"
               style="color:#2d4373; background-color: white; border-color: #2d4373; font-size: 18px;"><i
                    class="fa fa-plus-circle"
                    aria-hidden="true"></i> Voeg
                een product toe</a>

        </div>
    </div>

    <div class="col-md-12">
        <div class="row margin">
            <h4 class="col-xs-12 margin" style="height: 50px; text-align: left;  color:#2d4373;"><b>LEVERING</b></h4>
            <div class="col-xs-12">
                <div class="col-xs-4">
                    <button type="button" class="P1 btn btn-block btn-default btn-lg margin"
                            style="font-size: medium; color:#2d4373; background-color: white;" id="optionRadios1"
                            value="option1"><b>Afhalen (gratis)</b></button>
                </div>
                <div class="col-xs-4">
                    <button type="button" class="P1 btn btn-block btn-default btn-lg margin"
                            style="font-size: medium; color:#2d4373;  background-color: white;" id="optionRadios2"
                            value="option2"><b>Bezorgen (&#8364 50.00)</b></button>
                </div>
                <input type="hidden" name="optionsRadios" id="optionRadios"/>
            </div>
        </div>

        <div class="col-xs-12" id="bezorgDatumID" style="display: none">
            <div class="row margin">
                <div class="form-group ">
                    <label class="form-control"
                           style="border-style: none; color: #2d4373; background-color: transparent; ">Bezorgdatum</label>
                    <div class="form-group margin">
                        <div class="input-group date">
                            <div class="input-group-addon" style="color: #2d4373">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" name="datepicker" style="color: #2d4373"
                                   value="{{$order->bezorgdatumBegin ?? date("d/m/Y")}} - {{$order->bezorgdatumEnd ?? date("d/m/Y")}}"
                                   class="form-control pull-right" id="datepicker" required="required" readonly="">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="row margin">
                <h4 class="col-xs-12 margin" style="height: 50px; text-align: left;  color:#2d4373;"><b>BETALING</b>
                </h4>
                <div class="col-xs-3">
                    <button type="button" class="P1 btn btn-block btn-default btn-lg margin"
                            style="font-size: medium; color:#2d4373;  background-color: white;" id="Contant">
                        <b>Contant</b></button>
                </div>
                <div class="col-xs-3">
                    <button type="button" class="P1 btn btn-block btn-default btn-lg margin"
                            style="font-size: medium; color:#2d4373;  background-color: white;" id="Pin"><b>Pin</b>
                    </button>
                </div>
                <div class="col-xs-3">
                    <button type="button" class="P1 btn btn-block btn-default btn-lg margin"
                            style="font-size: medium; color:#2d4373;  background-color: white;" id="Factuur">
                        <b>Factuur</b></button>
                </div>
                <div class="col-xs-3">
                    <button type="button" class="P1 btn btn-block btn-default btn-lg margin"
                            style="font-size: medium; color:#2d4373;  background-color: white;" id="Bijlevering"><b>Bij
                            levering</b></button>
                </div>
                <input type="hidden" name="paymentType" id="paymentType"/>
            </div>
        </div>

        <div class="col-md-12">
            <div class="row margin">
                <table class="col-xs-12">
                    <tr>
                        <td class="col-xs-2">
                            <label class="form-control"
                                   style="border-style: none; background-color: transparent; color: #2d4373 ">AANBETALING</label>
                        </td>
                        <td class="col-xs-3">

                            <input type="number" name="Aanbetaling" step="0.01"
                                   value="{{isset($order->aanbetaling) ? $order->aanbetaling : 0}}"
                                   class="form-control @if($errors->has("Aanbetaling")) has-error @endif"
                                   id="Aanbetaling" placeholder="- &#8364" onkeyup="calculatePrices(event)"/>
                        </td>
                        <td class="col-xs-2"></td>
                        <td class="col-xs-2">
                            <label class="form-control"
                                   style="border-style: none; background-color: transparent; color: #2d4373">KORTING</label>
                        </td>
                        <td class="col-xs-3">
                            <input type="number" step="0.01" name="korting"
                                   value="{{isset($order->korting) ? $order->korting : 0}}"
                                   class="form-control @if($errors->has("korting")) has-error @endif" id="korting"
                                   placeholder="- &#8364" onkeyup="calculatePrices(event)"/>

                        </td>
                    </tr>
                </table>
            </div>
        </div>

        @if($order->inc_btw)

            <div class="col-md-12">
                <div class="row margin"
                     style="background-color: #4d5bae; border-radius: 15px; color: white; height: 100px;">
                    <table class="col-md-12" style=" margin-top: 20px">
                        <tr>
                            <th class="col-xs-2">
                                <h4 class="col-xs-12" style="font-weight: bold ">Totaalprijs</h4>
                                <h4 class="col-xs-12" style=" margin-top: -5px;">Exc. BTW</h4>
                            </th>
                            <th class="col-xs-2" id="totaalprijs"
                                name="totaalprijs">{{App\Helpers\PriceHelper::stringPriceWithTwoDigits($totalPrice)}}</th>
                            <th class="col-xs-2">
                                <h6 class="col-xs-12" style="font-weight: bold">BTW</h6>
                                <h6 class="col-xs-12" style=" margin-top: -5px;">21%</h6>
                            </th>
                            <th class="col-xs-2" id="priceLabelBTW"
                                name="product_prijs">{{App\Helpers\PriceHelper::stringPriceWithTwoDigits($totalBtw)}}</th>

                            <th class="col-xs-2">
                                <h6 class="col-xs-12" style="font-weight: bold">Totaalprijs</h6>
                                <h6 class="col-xs-12" style=" margin-top: -5px;">incl. BTW</h6>
                            </th>
                            <th class="col-xs-2" id="priceLabel"
                                name="product_prijs">
                                {{App\Helpers\PriceHelper::stringPriceWithTwoDigits($totalInc)}}
                            </th>
                            <input id="inputPriceProduct" value="{{$totalPrice}}" name="totaalprijs" type="hidden">
                            <input id="inputBTWProduct" name="product_btw" type="hidden" value="{{$totalBtw}}">
                            <input id="inputTotalIncProduct" name="product_total_inc_btw" type="hidden"
                                   value="{{$totalInc}}">
                        </tr>
                    </table>
                </div>
            </div>
        @else
            <div class="col-md-12">
                <div class="row margin"
                     style="background-color: #4d5bae; border-radius: 15px; color: white; line-height: 40px; text-align: center">
                    <div class="col-xs-8">
                        <h4 class="col-xs-12" style="font-weight: bold">Totaalprijs</h4>
                        <h5 class="col-xs-12" style=" margin-top: -5px;">Exc. BTW </h5>
                    </div>
                    <div class="col-xs-2">
                        <div class="form-group">
                            <label id="totaalprijs"
                                   class="form-group"
                                   style="font-size: 18px;">{{App\Helpers\PriceHelper::stringPriceWithTwoDigits($totalPrice)}}</label>
                            <input id="inputPriceProduct" name="totaalprijs" type="hidden" value="{{$totalPrice}}">
                        </div>
                    </div>
                </div>

            </div>
        @endif

        <div class="col-md-12">
            <div class="row margin">
                <input type="submit" value="VERDER" class="btn btn-block btn-default"
                       style="background-color:#e66505; color: white; border-radius: 10px; font-size: 18px;"/>
            </div>
        </div>


        {!! Form::close() !!}
        @stop
        @section("js")
            <script>

                //     Date picker
                $('#datepicker').daterangepicker({
                    "locale": {
                        "format": "DD/MM/YYYY",
                        "separator": " - ",
                        "applyLabel": "Toepassen",
                        "cancelLabel": "Annuleren",
                        "fromLabel": "Van",
                        "toLabel": "Tot",
                        "customRangeLabel": "Custom",
                        "weekLabel": "W",
                        "daysOfWeek": [
                            "zo",
                            "ma",
                            "di",
                            "woe",
                            "do",
                            "vr",
                            "za"
                        ],
                        "monthNames": [
                            "Januari",
                            "Februari",
                            "Maart",
                            "April",
                            "Mei",
                            "Juni",
                            "Juli",
                            "August",
                            "September",
                            "Oktober",
                            "November",
                            "December"
                        ],
                        "firstDay": 1
                    }
                });


                var temp = "<?php echo $order->betaalmethode; ?>";

                var paymentType = (temp !== "") ? temp : null;
                var deliveryInput = <?php echo $order->bezorgen; ?>;


                PaymentMethod(paymentType, false);
                optionRadios(deliveryInput === 1 ? "optionRadios2" : "optionRadios1");


                calculatePrices(event);


                $('#Contant').click(function () {
                    PaymentMethod($(this).attr('id'), true)
                });
                $('#Pin').click(function () {
                    PaymentMethod($(this).attr('id'), true)
                });
                $('#Factuur').click(function () {
                    PaymentMethod($(this).attr('id'), true)
                });
                $('#Bijlevering').click(function () {
                    PaymentMethod($(this).attr('id'), true)
                });

                $('#optionRadios1').click(function () {
                    optionRadios($(this).attr('id'))
                });
                $('#optionRadios2').click(function () {
                    optionRadios($(this).attr('id'))
                });

                //delete product button
                $(".deleteProduct").each(function (item) {
                    $(this).click(function (event) {
                        event.preventDefault();
                        var conf = confirm("Weet je zeker dat je deze prouct wilt verwijderen?");
                        if (conf === true) {
                            var data = $(this).data("product");
                            if (data === 0 || data === undefined)
                                return false;

                            $.get("/product/delete/" + data, function (respone) {
                                location.reload();
                            });
                        }

                    });
                });

                //edit product button
                $(".editProduct").each(function (item) {
                    $(this).click(function (event) {
                        event.preventDefault();
                        var data = $(this).data("product");
                        if (data === 0 || data === undefined)
                            return false;

                        window.location.href = "/product/edit/" + data;

                    });
                });

                $(".copyProduct").each(function (item) {
                    $(this).click(function (event) {
                        event.preventDefault();
                        var data = $(this).data("product");
                        if (data === 0 || data === undefined)
                            return false;

                        $.get("/product/copy/" + data, function (respone) {
                            console.log(respone);
                            window.location.href = "/product/edit/" + respone;
                        });


                    });
                });

                function PaymentMethod(id, post) {

                    if (paymentType !== "" && paymentType != null) {
                        var el = $("#" + paymentType);
                        el.css("background-color", "white");
                        el.css("color", "#2d4373");
                    }

                    paymentType = id;

                    var el = $("#" + paymentType);
                    el.css("background-color", "#4d5bae");
                    el.css("color", "white");
                    $("#paymentType").val(paymentType);
                }

                function optionRadios(id) {

                    var current = deliveryInput === 1 ? "optionRadios2" : "optionRadios1";
                    if (current !== "" && current != null) {
                        var el = $("#" + current);
                        el.css("background-color", "white");
                        el.css("color", "#2d4373");
                    }

                    current = id;
                    deliveryInput = current === "optionRadios2" ? 1 : 0;

                    var el = $("#" + current);
                    el.css("background-color", "#4d5bae");
                    el.css("color", "white");
                    $("#optionRadios").val(deliveryInput);
                    toggleDatePicker(deliveryInput);
                }

                function toggleDatePicker(show) {

                    if (show === 1) {
                        $("#bezorgDatumID").show();
                        $("#datepicker").show();
                    } else {
                        $("#bezorgDatumID").hide();
                        $("#datepicker").hide();
                    }
                    calculatePrices(event);
                }

                function calculatePrices(event) {
                    //korting
                    var korting = document.getElementById("korting").value;
                    if (korting.indexOf(",") !== -1) {
                        korting = korting.replace(",", ".");
                    }
                    if (korting === undefined || isNaN(korting) || korting === "") korting = 0;
                    korting = parseFloat(korting);

                    //aanbetaling
                    var aanbetaling = document.getElementById("Aanbetaling").value;
                    if (aanbetaling.indexOf(",") !== -1) {
                        aanbetaling = aanbetaling.replace(",", ".");
                    }
                    if (aanbetaling === undefined || isNaN(aanbetaling) || aanbetaling === "") aanbetaling = 0;

                    aanbetaling = parseFloat(aanbetaling);

                    var totalPriceCheck = <?php echo $totalPrice; ?>;


                    totalPriceCheck = parseFloat(totalPriceCheck);

                    var delivery = deliveryInput === 1 ? 50 : 0;
                    if (korting !== 0 && aanbetaling !== 0 && korting > totalPriceCheck - aanbetaling + delivery) {
                        alert("Aanbetaling + Korting kan niet hoger zijn dan de totale prijs!");
                        document.getElementById("korting").value = 0;
                        korting = 0;
                        document.getElementById("Aanbetaling").value = 0;
                        aanbetaling = 0;
                    }

                    if (korting > totalPriceCheck - aanbetaling + delivery) {
                        alert("Korting kan niet hoger zijn dan de totale prijs!");
                        document.getElementById("korting").value = 0;
                        korting = 0;
                    } else if (aanbetaling > totalPriceCheck - korting + delivery) {
                        alert("Aanbetaling kan niet hoger zijn dan de totale prijs!");
                        document.getElementById("Aanbetaling").value = 0;
                        aanbetaling = 0;
                    }


                    var totaalprijs = totalPriceCheck - korting;


                    var isIncl = <?php echo $isInclBTW; ?>;
                    //check if inclusive BTW
                    if (isIncl) {
                        var totaPrice = <?php echo $totalPrice; ?>;
                        totaPrice = parseFloat(totaPrice);
                        totaPrice += delivery;
                        totaPrice -= korting;
                        var BTW = (totaPrice / 1.21) * 0.21;
                        var tExBTW = parseFloat(totaPrice) - parseFloat(BTW);

                        var totalPriceInc = document.getElementById('priceLabel');
                        var totalBTW = document.getElementById('priceLabelBTW');

                        totalPriceInc.innerHTML = priceWithComma(priceWithTwoDigits(totaPrice));
                        totalBTW.innerHTML = priceWithComma(priceWithTwoDigits(BTW));

                        document.getElementById("inputBTWProduct").value = BTW;
                        document.getElementById("inputTotalIncProduct").value = totaPrice;

                        document.getElementById("totaalprijs").innerHTML = priceWithComma(priceWithTwoDigits(tExBTW));
                        document.getElementById("inputPriceProduct").value = tExBTW;

                    } else {

                        var num = priceWithComma(priceWithTwoDigits(totaalprijs + delivery));
                        document.getElementById("totaalprijs").innerHTML = num;
                        document.getElementById("inputPriceProduct").value = totaalprijs + delivery;
                    }

                }


                function priceWithComma(price) {
                    var stringPrice = "" + price;
                    if (stringPrice.indexOf(".") !== -1)
                        stringPrice = stringPrice.replace(".", ",");

                    return "€ " + stringPrice;
                }

                function priceWithTwoDigits(price) {
                    var numberPrice = Number(price);
                    return numberPrice.toFixed(2);
                }

            </script>

@stop

