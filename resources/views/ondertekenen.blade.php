@extends('layouts.app')
@section('css')

    {{--here css files....--}}
    <link href="{{ asset('css/signature_pad.css') }}" rel="stylesheet">
    <link href="{{ asset('css/ie9.css') }}" rel="stylesheet">
@stop
@section('content')
    {!! Form::open(array('id' =>'my-form', 'files' => true)) !!}

    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-xs-12" style="background-color: #252e65; width: 100%">
                    <div class="col-xs-3 col-sm-1">
                        <a href='{{route("FinalOverzicht")}}' class="btn btn-block btn-default btn-md margin"
                           style="background-color: #252e65"><i class="fa fa-chevron-left" aria-hidden="true"
                                                                style="color: white"></i></a>
                    </div>
                    <div class="col-xs-6 col-sm-10">
                        <h5 class="col-xs-12 margin"
                            style="color: white; font-weight: bold; line-height: 40px;  display:flex; justify-content: center; align-items:center">
                            Ondertekenen</h5>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12"
             style="display:flex; justify-content: center; align-items:center; margin-top: 10px; margin-botton: 10px">
            <img src="{{URL::asset('/uploads/' . $company .".png")}}" alt=""
                 style=" height: 70px; margin-bottom: 10px;"/>
        </div>

        @if ($errors->any())
            <div class="alert alert-danger margin col-md-12">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="col-md-12">
            <div class="row margin ">
                <div class="col-xs-12">
                    <div class="form-group">
                        <div class="col-xs-10">
                            <label class="form-control"
                                   style="font-size: 18px; border-style: none; background-color: transparent; color: #2d4373;">Voorwaarden</label>
                        </div>
                        <div class="col-xs-2">
                            <label class="switch"><input type="checkbox" name="terms"><span class="slider round"></span></label>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <h6 class="title col-md-12" style="font-size: 18px; color: #2d4373;">Ik ga akkoord met de algemene
            voorwaarden</h6>
        <div class="col-md-12">
            <div id="signature" class="signature-pad"></div>
        </div>


        <div class="col-md-12">
            <div class="row margin">
                <input type="submit" id="sbmit" value="Bestelling afronden" class="btn btn-block btn-default"
                       style="background-color:#e66505; color: white; border-radius: 10px; margin-top: 10px; font-size: 18px;"/>
            </div>
        </div>
        <div class="col-md-12">
            <div class="row margin">
                <div class="col-xs-4 col-sm-4"></div>
                <div class="col-xs-4 col-sm-4">
                    <button class="btn btn-block btn-default"
                            style="background-color:#252e65; color: white; font-weight: bold;" id="clear_Sign">
                        Handtekening ongedaan maken
                    </button>
                </div>
            </div>
        </div>
        <input type="hidden" name="signature" id="mySig">
        {!! Form::close() !!}
        @stop
        @section('js')

            <script src="{{ asset('/js/jSignature.min.js') }} " rel="javascript" type="text/javascript"></script>
            <script>
                $(document).ready(function () {
                    $("#signature").jSignature();
                });

                $("#clear_Sign").click(function () {
                    event.preventDefault();
                    $("#signature").jSignature("reset");
                })

            </script>
            <script>

                $('#sbmit').on("click", function (e) {
                    e.preventDefault(); // cancel the actual submit

                    /* do what you want with the form */

                    // Should be triggered on form submit
                    var d = $("#signature").jSignature("getData", "native");
                    // if there are more than 2 strokes in the signature
                    // or if there is just one stroke, but it has more than 20 points
                    if (d.length > 2 || (d.length === 1 && d[0].x.length > 20)) {
                        var datapair = $("#signature").jSignature("getData");
                        $('#mySig').val(datapair);
//            download(data, "signature.jpg");
                        console.log("called");
                        $('#my-form').submit();

                    } else {
                        alert("no sign");
                    }
                });

            </script>
@stop

