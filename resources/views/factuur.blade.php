<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>FV factuur CSS</title>
    {{--    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet"--}}
    {{--          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">--}}
    <link href="https://use.typekit.net/yta0sjg.css" rel="stylesheet">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
          integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <style>
        /* CSS Document */

        body {
            width: 800px;
            font-family: 'canada-type-gibson', sans-serif;
        }

        strong {
            font-weight: 600 !important;
        }


        .row {
            width: 800px;
        }

        .sub {
            font-size: 14px;
            line-height: 20px;
            font-weight: 400 !important;

        }


        .invoice {
            width: 800px;
            margin: 20px 50px 20px 50px;
        }

        .logo {
            float: right;
            margin-top: -10px;
            height: auto;
            max-width: 80%;
        }

        .title {
            font-size: 28px;
            font-weight: 600;
            padding-bottom: 15px;
            line-height: 20px;
        }

        .sub-title {
            font-size: 18px;
            font-weight: 600 !important;
            padding-bottom: 10px;
        }

        .bedrijfsInfo {
            font-size: 12px;
            text-align: right;
            line-height: 18px;
            font-weight: 400;
        }

        .invoice-info {
            margin-left: 20px;
        }

        .invoice-info-middle {
            margin-left: 19px;
        }

        .invoice-info-last {
            margin-left: 58px;
        }

        /* head */

        .head {
            margin-bottom: -20px;
        }


        /* table product */
        .table {
            width: 100%;
        }

        .table-total {
            margin-bottom: 30px;
        }

        .td-product {
            width: 58%;
        }


        .BTW {
            font-size: 14px;
            line-height: 20px;
            font-weight: 400;
            padding-right: -300px;
            text-align: right;
        }

        .table th {
            padding: .75rem;
            vertical-align: top;
            border-bottom: 1px solid #dee2e6;
        }

        .table td {
            padding: .75rem;
        }

        .total-pay {
            font-weight: 600;
        }

        /* table total */


        /* conditions */


        ul {
            list-style-type: none;
            padding: 0;
        }


        li {
            padding-left: 1.3em;
        }

        li:before {
            content: "\f058"; /* FontAwesome Unicode */
            font-family: FontAwesome;
            display: inline-block;
            margin-left: -1.3em; /* same as padding-left set on li */
            width: 1.3em; /* same as padding-left set on li */
        }

        .light_th {
            font-weight: 600 !important;
            text-align: left;
        }
    </style>
</head>

<body class="invoice">

<table style="width: 800px;  display: grid;">
    <tbody>
    <tr>
        <td style="width: 450px;">
            <h1 class="title">Factuur</h1>
            <p class="sub">
                <strong>Factuurdatum</strong> <span
                    class="invoice-info">{{date('d-m-Y', strtotime($order->updated_at))}}</span><br>
                <strong>Ordernummer</strong> <span class="invoice-info-middle">{{$order->id}}</span><br>
                <strong>Verkoper</strong><span class="invoice-info-last">{{$employee}}</span></p>
        </td>
        <td>
            <img class="logo" src="{{URL::asset('/logo.png')}}"
                 alt="fabrieksverkoop"/>
        </td>
    </tr>
    </tbody>
</table>
<table style="width: 800px;">
    <tbody>
    <tr>
        <td>
            <h3 class="sub"><strong>Je
                    gegevens</strong><br>{{$order->naam}}
                <br>{{$order->straatnaam . ' ' . $order->huisnummer}}<br>{{$order->postcode}}
                {{$order->plaats}}<br>{{$order->country}}<br><br>{{$order->tel}}<br>{{$order->email}}<br><br>
            </h3>
        </td>
        <td>
            <h3 class="sub">
                <strong>Afleveradres</strong><br>@if($order->straatnaamaflever != null && $order->straatnaamaflever != "") {{$order->straatnaamaflever . ' ' . $order->huisnummeraflever }} @else {{$order->straatnaam . ' ' . $order->huisnummer}} @endif
                <br>@if($order->postcodeaflever != null && $order->postcodeaflever != "") {{$order->postcodeaflever }} @else {{$order->postcode}} @endif
                @if($order->plaatsaflever != null && $order->plaatsaflever != "") {!!  $order->plaatsaflever . "<br>" . $order->countryaflever!!} @else {!! $order->plaats . '<br>' . $order->country!!} @endif
                <br><br><strong>Bezorging</strong><br>Afleverdatum: tussen<br>{{$order->bezorgdatumBegin}}
                - {{$order->bezorgdatumEnd}}
            </h3>
        </td>
        <td>
            <h3 class="bedrijfsInfo"><br>Fabrieksverkoop B.V.<br>
                Schering 17a<br>8281 JW<br> GENEMUIDEN<br>www.fabrieksverkoop.nl<br><br>KVK Zwolle, 05055285<br>BTW NL0049.30.447.B01<br>IBAN NL43INGB0658423703<br>BIC INGBNL2A</h3>
        </td>
    </tr>
    </tbody>
</table>
@if($isOfferte)
    <h3 class="sub-title" style="width: 800px;">Overzicht van je offerte aanvraag</h3>
@else
    <h3 class="sub-title">Overzicht van je bestelling</h3>
@endif
<div class="table">
    <div class="row">
        <table class="table table-borderless">
            <thead>
            <tr class="border-bottom">
                <th class="light_th" scope="col">Artikel</th>
                <th class="light_th" scope="col">Aantal</th>
                <th class="light_th" scope="col">Prijs per stuk</th>
                <th class="light_th"
                    scope="col">@if($isInclBTW) {{"Prijs incl. BTW"}} @else {{"Prijs excl. BTW"}} @endif</th>
            </tr>
            </thead>
            <tbody>

            @foreach($products as $key => $product)
                <tr>
                    <td class="td-product">{{$product->product_type}}<br>
                        {{$product->description}}<br>
                        @if($product->cutMeasures != null && count(json_decode($product->cutMeasures)) > 0)
                            <?php $count = 1; ?>
                            @foreach (json_decode($product->cutMeasures) as $arr)

                                Maat {{$count}} - {{$arr[0] . ': ' . $arr[1] .' x '. $arr[2] .'cm '}}<br>
                                <?php $count += 1;?>
                            @endforeach
                        @endif
                    </td>
                    <td>{{$product->count . ' ' . $product->unit}}</td>
                    <td>{{App\Helpers\PriceHelper::stringPriceWithTwoDigits($product->product_price)}}</td>
                    <td>{{App\Helpers\PriceHelper::stringPriceWithTwoDigits($product->pricePerUnit == 1 ? ($product->product_price * $product->count) : $product->product_price)}}</td>
                </tr>
                <?php foreach (json_decode($product->accessories) as $arr) {
                if (($arr[2] == 0)) continue; ?>
                <tr>
                    <td>{{$arr[0]}}</td>
                    <td>{{$arr[2]}} x</td>
                    <td>{{App\Helpers\PriceHelper::stringPriceWithTwoDigits($arr[1])}}</td>
                    <td>{{App\Helpers\PriceHelper::stringPriceWithTwoDigits($arr[1] * $arr[2])}}</td>
                </tr>
                <?php }?>

            @endforeach

            @if($product->plintenInfo != null && $product->plintenInfo !== "Geenplinten")
                <tr>
                    <td>{{$product->plintenInfo}}<br> {{$product->plintenDetails}}</td>
                    <td>{{$product->plintCount . ' m1 '}}</td>
                    <td>{{App\Helpers\PriceHelper::stringPriceWithTwoDigits($product->plintenPrice)}}</td>
                    <td>{{App\Helpers\PriceHelper::stringPriceWithTwoDigits($product->plintenPrice * $product->plintCount)}}</td>
                </tr>
            @endif

            @if($order->bezorgen == 1)
                <tr>
                    <td>Bezorgen tussen {{$order->bezorgdatumBegin}} - {{$order->bezorgdatumEnd}}</td>
                    <td>1 x</td>
                    <td>{{App\Helpers\PriceHelper::stringPriceWithTwoDigits($product->plintenPrice)}}</td>
                    <td>{{App\Helpers\PriceHelper::stringPriceWithTwoDigits($product->plintenPrice * $product->plintCount)}}</td>
                </tr>
            @endif
            </tbody>
        </table>
    </div>
</div>

<hr>
<div class="row table-total" style="width: 100%; display: inline-block; float: left">
    <div style="width: 33%; display: inline-block">
        <table class="table table-borderless table-btw">
            <tbody>
            <tr>
                <td>Exclusief BTW</td>
                <td>@if($isInclBTW){{App\Helpers\PriceHelper::stringPriceWithTwoDigits(($order->order_total_inc_btw + $order->korting) / 1.21)}} @else{{App\Helpers\PriceHelper::stringPriceWithTwoDigits($order->totaalprijs)}}@endif</td>
            </tr>
            @if($isInclBTW)
                <tr>
                    <td> 21%</td>

                    <td>{{App\Helpers\PriceHelper::stringPriceWithTwoDigits(($order->order_total_inc_btw + $order->korting)- ($order->order_total_inc_btw + $order->korting) / 1.21)}}</td>
                </tr>
                <tr class="border-top" style="border-top: 1px solid lightgrey !important;">
                    <td style="border-top: 1px solid lightgrey !important;">Totaal incl. BTW</td>
                    <td style="border-top: 1px solid lightgrey !important;">{{App\Helpers\PriceHelper::stringPriceWithTwoDigits(($order->order_total_inc_btw+ $order->korting))}}</td>
                </tr>
            @endif
            </tbody>

        </table>
    </div>
    <div style="width: 33%; display: inline-block; float: right">
        <table class="table table-borderless">
            <tbody>
            <tr>
                <td>Subtotaal</td>
                @if($isInclBTW)
                    <td>{{App\Helpers\PriceHelper::stringPriceWithTwoDigits($order->order_total_inc_btw + $order->korting)}}</td>
                @else
                    <td>{{App\Helpers\PriceHelper::stringPriceWithTwoDigits($order->totaalprijs + $order->korting)}}</td>
                @endif

            </tr>
            <tr>
                <td> Korting</td>
                <td> - {{App\Helpers\PriceHelper::stringPriceWithTwoDigits($order->korting)}}</td>
            </tr>
            <tr class="border-top">
                <td class="total-pay" style="border-top: 1px solid lightgrey !important;">Totaal</td>
                @if($isInclBTW)
                    <td class="total-pay"
                        style="border-top: 1px solid lightgrey !important;">{{App\Helpers\PriceHelper::stringPriceWithTwoDigits($order->order_total_inc_btw )}}</td>
                @else
                    <td class="total-pay"
                        style="border-top: 1px solid lightgrey !important;">{{App\Helpers\PriceHelper::stringPriceWithTwoDigits($order->totaalprijs )}}</td>
                @endif
            </tr>
            </tbody>

        </table>
    </div>
</div>

<table style="width: 100%;">
    <tbody>
    <tr>
        <td style="width: 50%;">

            <small><strong>Leveringscondities: belangrijk voor Aﬂevering / Leggen</strong>
                <ul>
                    <li>De vloer dient leeg, schoon en volledig vlak te zijn.</li>
                    <li>Fabrieksverkoop is niet aansprakelijk voor schade aan meubels/voorwerpen tijdens bezorging /
                        leggen.
                    </li>
                    <li>Bij aﬂevering of voor oplevering dient de betaling volledig voldaan te zijn.</li>
                    <li>Wil je de legopdracht annuleren? Doe dit dan minimaal 48 uur van te voren, anders worden er
                        kosten
                        in
                        rekening gebracht.
                    </li>
                    <li>Meer informatie op fabrieksverkoop.nl/algemene-voorwaarden/</li>
                </ul>

            </small>

        </td>
        <td style="width: 10%;"></td>
        <td
            style=" width: 40%; border: 1px solid lightgrey; padding-bottom: 20px; padding-top: 20px;
        ">

            <table>
                <tbody>
                <tr>
                    <td style="padding: 10px;">Aanbetaling</td>
                    <td>{{App\Helpers\PriceHelper::stringPriceWithTwoDigits($order->aanbetaling)}}</td>
                </tr>
                <tr>
                    <td style="padding: 10px;" class="total-pay">Nog te betalen</td>
                    @if($isInclBTW)
                        <td class="total-pay">{{App\Helpers\PriceHelper::stringPriceWithTwoDigits($order->order_total_inc_btw - $order->aanbetaling)}}</td>
                    @else
                        <td class="total-pay">{{App\Helpers\PriceHelper::stringPriceWithTwoDigits($order->totaalprijs - $order->aanbetaling)}}</td>
                    @endif

                </tr>
                </tbody>

            </table>
            <small style="padding: 10px; "><strong>Bij betaling per bank ordernummer
                    vermelden</strong></small>
        </td>
    </tr>
    </tbody>


</table>
</body>
</html>
