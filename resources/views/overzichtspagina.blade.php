@extends('layouts.app')
@section('css')

    {{--here css files....--}}
@stop
@section('content')
    {!! Form::open(array('id' => 'formPost')) !!}

        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-xs-12" style="background-color: #252e65; width: 100%">
                        <div class="col-xs-3 col-sm-1">
                            <a href="{{route("home")}}" class="btn btn-block btn-default btn-md margin" style="background-color: #252e65"><i class="fa fa-chevron-left" aria-hidden="true" style="color: white"></i></a>
                        </div>
                        <div class="col-xs-6 col-sm-10">
                            <h4 class="col-xs-12 margin" style="color: white; font-weight: bold; line-height: 40px; display:flex; justify-content: center; align-items:center; margin-top: 10px">Overzicht Orders</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <h4 class="col-xs-12" style="text-align: center; color:#2d4373;margin-top: 20px;"><b>Overzicht Orders</b></h4>

        <div class="col-md-12">
            <table id="example1" class="table table-striped dataTable" role="grid" style="color: #2d4373;" aria-describedby="example1_info">
                <thead>
                <tr role="row">
                    <th class="sorting_asc col-md-1" tabindex="6" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="">Order Datum</th>
                    <th class="sorting col-md-2" tabindex="1" aria-controls="example1" rowspan="1" colspan="1" aria-label="" >Klant Naam</th>
                    <th class="sorting col-md-1" tabindex="1" aria-controls="example1" rowspan="1" colspan="1" aria-label="" >Levering</th>
                    <th class="sorting col-md-2" tabindex="2" aria-controls="example1" rowspan="1" colspan="1" aria-label="" >Plaats</th>
                    <th class="sorting col-md-2" tabindex="3" aria-controls="example1" rowspan="1" colspan="1" aria-label="" >Totaal Bedrag</th>
                    <th class="sorting col-md-2" tabindex="4" aria-controls="example1" rowspan="1" colspan="1" aria-label="" >Status</th>
                    <th class="sorting col-md-2" tabindex="5" aria-controls="example1" rowspan="1" colspan="1" aria-label="">Betaling</th>
                    <th class="sorting col-md-2" tabindex="5" aria-controls="example1" rowspan="1" colspan="1" aria-label="">Obdrachtbevesting</th>
                </tr>
                </thead>
                <tbody>
                <?php $count = 0; ?>
                @foreach($orders as $order)
                    <tr id="{{$order->id}}">

                        <td data-id="{{$order->id}}" class="clickableTd"><span style="display:none; ">{{$order->updated_at}}</span> {{date('d-m-Y', strtotime($order->updated_at))}}</td>
                        <td data-id="{{$order->id}}" class="clickableTd">{{$order->naam}}</td>
                        <td data-id="{{$order->id}}" class="clickableTd">{{$order->bezorgen === 1 ? "Ja" : "Nee"}}</td>
                        <td data-id="{{$order->id}}" class="clickableTd">{{$order->plaats}}</td>
                        <td data-id="{{$order->id}}" class="clickableTd">@if($order->inc_btw) {{App\Helpers\PriceHelper::stringPriceWithTwoDigits($order->order_total_inc_btw)}} @else {{App\Helpers\PriceHelper::stringPriceWithTwoDigits($order->totaalprijs)}}@endif</td>
                        <td>
                            @if($order->status_type === "inbehandeling")
                                <span style="display:none; ">1</span>
                            @elseif($order->status_type === "ingepland")
                                <span style="display:none; ">2</span>
                            @elseif($order->status_type === "voltooid")
                                <span style="display:none; ">3</span>
                            @else
                                <span style="display:none; ">4</span>
                            @endif
                            <select id="status_{{$count}}" name="status_type" class="status-select form-control" style="font-weight: bolder; width: 100%; height: 50px; color:#2d4373; border-style: none; text-align: center" required="required" data-content="{{$order->id}}" data-count = {{$count}}>>
                            <option value="ingepland" {{$order->status_type == "ingepland" ? "selected" : ""}}>Ingepland</option>
                            <option value="inbehandeling" {{$order->status_type == "inbehandeling" ? "selected" : ""}}>In behandeling</option>
                            <option value="voltooid" {{$order->status_type == "voltooid" ? "selected" : ""}}>Voltooid</option>
                            <option value="geannuleerd" {{$order->status_type == "geannuleerd" ? "selected" : ""}}>Geannuleerd</option>
                            </select>
                            {{--{{Form::select('status_type', $list, null, ['id'=>'status'], 'ingepland' => ['background-color'=>'orange'], 'inbehandeling'=>['background-color'=>'yellow'], 'vooltooid'=>['background-color'=>'green'], 'geannuleerd'=>['background-color'=>'red'])}}--}}

                        </td>


                        <td>
                            @if($order->payment_type === "betaald")
                                <span style="display:none; ">1</span>
                            @elseif($order->payment_type === "deelsbetaald")
                                <span style="display:none; ">2</span>
                            @else
                                <span style="display:none; ">3</span>
                            @endif
                            <select id="payment_{{$count}}" name="payment_type" class="payment-select form-control" style="width: 100%; height: 50px; color:#2d4373;; border-style: none; text-align: center" required="required" data-content="{{$order->id}}" data-count = {{$count}}>
                                <option value="nietbetaald" {{$order->payment_type == "nietbetaald" ? "selected" : ""}}>Niet betaald</option>
                                <option value="betaald" {{$order->payment_type == "betaald" ? "selected" : ""}}>Betaald</option>
                                <option value="deelsbetaald" {{$order->payment_type == "deelsbetaald" ? "selected" : ""}}>Deels betaald</option>
                            </select>
                        </td>
                        <td data-id="{{$order->id}}" class="clickableTd"><a href="{{ route('download-pdf',['id'=>$order->id]) }}" class="btn btn-block btn-default" style="background-color:#252e65; color: white; border-radius: 10px;">Pakbon afdrukken <i class="fa fa-chevron-right" aria-hidden="true" style="color: white;  border-radius: 10px"></i></a></td>

                    </tr>
                    <?php $count += 1; ?>
                @endforeach
                </tbody>
            </table>
        </div>
    {!! Form::close() !!}
@stop

@section("js")

    <script>
        $(function () {
            $('#example1').DataTable({
                'column': [

                ],
            })
        });

        $('.clickableTd').click(function (event) {
            var id = $(this).data('id');
            window.location.href = "/productoverzicht/" + id;
        });


        $('.status-select').each(function (item) {

            setSelectColor($(this));

            $(this).change(function () {
                var orderID = $(this).data('content');
                var count = $(this).data('count');

                var id = $(this).attr('id');
                var payment = $('#payment_' + count).val();

                var current = $(this);
                try {

                    $.ajax({
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        url: '/overzichtspagina',
                        dataType: 'json',
                        type: 'POST',
                        data: {
                            status_type: $(this).val(),
                            payment_type: payment,
                            order_id: orderID},
                        success: function (data) {
                            console.log(data);
                            setSelectColor(current);
                        },
                        error: function (data, textStatus, errorThrown) {
                            console.log(data);
                            setSelectColor(current);
                        },
                    });
                } catch (e) {
                    alert(e);
                    setSelectColor(current);
                }
            });

        });

        $('.payment-select').each(function (item) {
            $(this).change(function () {
                var orderID = $(this).data('content');
                var count = $(this).data('count');

                var id = $(this).attr('id');
                var status = $('#status_' + count).val();

                try {

                    $.ajax({
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        url: '/overzichtspagina',
                        dataType: 'json',
                        type: 'POST',
                        data: {
                            status_type: status,
                            payment_type: $(this).val(),
                            order_id: orderID},
                        success: function (data) {
                            console.log(data);
                        },
                        error: function (data, textStatus, errorThrown) {
                            console.log(data);

                        },
                    });
                } catch (e) {
                    alert(e);
                }
            });
        });

        function setSelectColor(item) {

            var value = item.val();
            switch (value) {
                case 'inbehandeling':
                    item.css("background-color", "yellow");
                    item.css("color", "black");
                    break;
                case 'voltooid':
                    item.css("background-color", "green");
                    item.css("color", "black");

                    break;
                case 'ingepland':
                    item.css("background-color", "orange");
                    item.css("color", "black");
                    break;
                case 'geannuleerd':
                    item.css("background-color", "red");
                    item.css("color", "black");
                    break;

            }
        }
    </script>
@stop
