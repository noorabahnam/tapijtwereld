<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html charset=UTF-8"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js" rel="javascript"
            type="text/javascript"></script>
    <title>TapijtWereld</title>

</head>
<body
    style="background-color: #f1f3f9 !important; margin: 0 !important; padding: 50px !important; width: 100% !important; font-family: canada-type-gibson, sans-serif; font-style: normal; font-weight: 900;">
<table style="width: 100%;">
    <tr>
        <td>
            <img src="{{URL::asset('/uploads/' . $company .".png")}}" alt="" width="150"
                 style=" max-height: 70px; float: right"/>
        </td>
    </tr>
    <tr>
        <td style="">
            <p
                style="color: #2d4373 !important; font-weight: bold; margin: 0; mso-line-height-rule:exactly; ">
                BEDANKT VOOR JE BESTELLING! BIJ FABRIEKSVERKOOP
            </p>
        </td>

    </tr>
    <td>
        <h6
            style="font-weight: bold; color: #2d4373 !important; mso-line-height-rule:exactly; margin: 0;">
            Beste &nbsp;{{$order->naam}},</h6>
    </td>
    <tr>
        <td style="border-style: none; font-size: small; background-color: transparent; color: #2d4373">Uw
            bestelling met ordernummer {{$order->id}} is verwerkt. Uw factuur en de gegevens van Uw bestelling vindt
            u in de bijlage van deze
            mail.

            <div class="col-xs-6" style="color: #2d4373">
                <p style="color: #2d4373">Bestelnummer:&nbsp;&nbsp;<b>{{$order->id}}</b></p>
                <p style="color: #2d4373">
                    Datum:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>{{date('d-m-Y', strtotime($order->updated_at))}}</b>
                </p>
                <p style="color: #2d4373">
                    Verkoper:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>{{$employee}}</b></p>
            </div>
        </td>
    </tr>


    <tr>
        <td>
            <table class="col-xs-12 margin" style="color:#2d4373;border: #2d4373 1px solid;">
                <tr style="font-weight: bold; font-size: medium">
                    <th class="col-xs-6" colspan="2">Uw gegevens</th>

                    <th class="col-xs-6" colspan="2">Aflevergegevens</th>

                </tr>

                <tr>
                    <td class="col-xs-2">Naam:</td>
                    <td class="col-xs-4">{{$order->naam}}</td>
                    <td class="col-xs-3">Naam:</td>
                    <td class="col-xs-3">{{$order->naam}}</td>
                </tr>
                <tr>
                    <td class="col-xs-2">Adres:</td>
                    <td class="col-xs-4">{{$order->straatnaam . ' ' . $order->huisnummer}}</td>
                    <td class="col-xs-3">Adres:</td>
                    <td class="col-xs-3">@if($order->straatnaamaflever != null && $order->straatnaamaflever != "") {{$order->straatnaamaflever . ' ' . $order->huisnummeraflever }} @else {{$order->straatnaam . ' ' . $order->huisnummer}} @endif </td>
                </tr>
                <tr>
                    <td class="col-xs-2">Postcode:</td>
                    <td class="col-xs-4">{{$order->postcode}}</td>
                    <td class="col-xs-3">Postcode:</td>
                    <td class="col-xs-3">@if($order->postcodeaflever != null && $order->postcodeaflever != "") {{$order->postcodeaflever }} @else {{$order->postcode}} @endif </td>

                </tr>
                <tr>
                    <td class="col-xs-2">Plaats:</td>
                    <td class="col-xs-4">{{$order->plaats. '-' . $order->country}}</td>
                    <td class="col-xs-3">Plaats:</td>
                    <td class="col-xs-3">@if($order->plaatsaflever != null && $order->plaatsaflever != "") {{$order->plaatsaflever . "-" . $order->countryaflever}} @else {{$order->plaats . '-' . $order->country}} @endif </td>

                </tr>
                <tr>
                    <td class="col-xs-2">Telefoon:</td>
                    <td class="col-xs-4">{{$order->tel}}</td>
                    <td class="col-xs-2">Telefoon:</td>
                    <td class="col-xs-4">{{$order->tel}}</td>
                </tr>
                <tr>
                    <td class="col-xs-2">E-mail:</td>
                    <td class="col-xs-4">{{$order->email}}</td>
                    <td class="col-xs-2">E-mail:</td>
                    <td class="col-xs-4">{{$order->email}}</td>
                </tr>
            </table>
        </td>
    </tr>

    <tr>
        <td>
            @if($isOfferte)
                <p class="col-md-12 margin"
                   style="color: #2d4373; font-weight: bold; text-align: left; margin-top: 20px; background-color: transparent;">
                    Overzicht van uw offerte!</p>
            @else
                <p class="col-md-12 margin"
                   style="color: #2d4373; font-weight: bold; text-align: left; margin-top: 20px; background-color: transparent;">
                    Overzicht van uw bestelling</p>
            @endif
        </td>
    </tr>


    <tr>
        <td>
            @foreach($products as $key => $product)
                <table style="width: 100%;">
                    <tr>
                        <td class="col-xs-3">
                            <p class="col-xs-12"
                               style="color: #2d4373; font-weight: bold">{{$product->product_type}}</p>
                            <p class="col-xs-12"
                               style="color: #2d4373; margin: 0; ">{{$product->description}}</p>
                            @foreach (json_decode($product->cutMeasures) as $arr)
                                <p class="col-xs-12"
                                   style="color: #2d4373; margin: 0; ">
                                    - {{$arr[0] . ': ' . $arr[1] .' x '. $arr[2] .'cm '}}</p>
                            @endforeach
                        </td>
                        <td class="col-xs-3">
                            <p class="col-xs-12"
                               style="color: #2d4373 !important;">{{$product->count . ' ' . $product->unit}}</p>
                        </td>

                        <td class="col-xs-3" style="color: #2d4373 !important;">
                            {{App\Helpers\PriceHelper::stringPriceWithTwoDigits($product->pricePerUnit == 1 ? ($product->product_price * $product->count) : $product->product_price)}}
                        </td>
                    </tr>

                    <tr>
                        <td colspan="5">
                            <?php foreach (json_decode($product->accessories) as $arr) {
                            if (($arr[2] == 0)) continue; ?>

                            <table>
                                <tr class="col-xs-2"></tr>
                                <tr class="col-xs-4">
                                    <td class="col-xs-12"
                                        style="color: #2d4373; margin: 0; font-size: 15px; ">{{$arr[0]}}</td>
                                </tr>
                                <tr class="col-xs-3">
                                    <td class="col-xs-12"
                                        style="color: #2d4373 !important; margin: 0; font-size: 15px;"> {{App\Helpers\PriceHelper::stringPriceWithTwoDigits($arr[1]) . ' x ' . $arr[2]}}</td>
                                </tr>
                                <tr class="col-xs-3">
                                    <td>
                                        <p style="border-style: none; color: #2d4373 !important; font-size:
                                        15px;">{{App\Helpers\PriceHelper::stringPriceWithTwoDigits($arr[1] * $arr[2])}}</p>
                                    </td>
                                </tr>

                            </table>
                            <?php } ?>
                        </td>
                    </tr>

                    @if($product->plintenInfo != null && $product->plintenInfo !== "Geenplinten")
                        <tr>
                            <td class="col-xs-2"></td>

                            <td class="col-xs-4">
                                <h5 class="col-xs-12"
                                    style="color: #2d4373; margin: 0; ">{{$product->plintenInfo}}</h5>
                                <h5 class="col-xs-12"
                                    style="color: #2d4373; margin: 0; ">{{$product->plintenDetails}}</h5>
                                <h5 class="col-xs-12"
                                    style="color: #2d4373; margin: 0; ">{{'€ '. $product->plintenPrice . ' per meter '}}</h5>
                            </td>
                            <td class="col-xs-3">
                                <h5 class="col-xs-12"
                                    style="color: #2d4373;">{{$product->plintCount . ' m1 '}}</h5>
                            </td>

                            <td class="col-xs-3">
                                <p class="form-control"
                                   style="border-style: none; color: #2d4373;">{{App\Helpers\PriceHelper::stringPriceWithTwoDigits($product->plintenPrice * $product->plintCount)}}</p>
                            </td>
                        </tr>
                    @endif
                    <tr>
                        <td class="col-xs-2"></td>

                        <td class="col-xs-4">
                            <h5 class="col-xs-12" style="color: #2d4373; margin: 0; ">Legservice</h5>
                        </td>
                        <td class="col-xs-3">
                            <h5 class="col-xs-12"
                                style="color: #2d4373;">{{$product->legService == 0 ? "Nee" : "Ja"}} </h5>
                        </td>

                        <td class="col-xs-3">
                            <div class="form-group">
                                <label class="form-control"
                                       style="border-style: none; color: #2d4373;">{{$product->legService == 0 ? '' :App\Helpers\PriceHelper::stringPriceWithTwoDigits($product->legServicePrice)}}</label>
                            </div>
                        </td>
                    </tr>

                </table>

            @endforeach


            @if($order->bezorgen == 1)
                <table class="col-md-12">
                    <tr>
                        <td class="col-xs-2">
                            <img class="img-responsive" src="{{URL::asset('/uploads/Bezorgservice.png')}}"
                                 alt="" width="50" style=" margin-top: 10px"/>
                        </td>

                        <td class="col-xs-3">
                            <h4 class="col-xs-12" style="color: #2d4373 !important; font-weight: bold">Bezorging</h4>
                        </td>
                        <td class="col-xs-4">
                            <h6 class="col-md-12 margin" style="color: #2d4373 !important;">Afleverdatum:
                                tussen {{$order->bezorgdatumBegin}}-{{$order->bezorgdatumEnd}}</h6>
                        </td>
                        <td class="col-xs-3">
                            <label class="col-md-12 margin" style="border-style: none; color: #2d4373;">&#8364
                                50,00</label>
                        </td>
                    </tr>
                </table>
            @endif

            <h6 class="col-xs-12" style="font-weight: bold; color: #2d4373 !important;">Belangrijk voor bezorging /
                legging</h6>
            <p style="list-style-position: inside; color: #2d4373 !important;">
                - De vloer dient leeg te zijn<br/>
                - Vloer is vlak en schoon<br/>
                - Fabriekverkoop is niet aansprakelijk voor schade aan meubels/voorwerpen tijdens
                bezorging / leggen
                <br/>
                - Betalling voldaan zijn voor aflevering<br/>
            </p>
            <p class="col-md-6" style="font-size: 12px; color: #2d4373 !important; ">Meer informatie op
                fabrieksverkoopgenemuiden.nl/bezorging</p>
            <table class="col-xs-12">
                @if($isInclBTW)
                    <tr>
                        <td class="col-xs-3" style="color: #2d4373 !important;">Totaalprijs (excl BTW)</td>

                        <td class="col-xs-3"
                            style="padding-left: 35px; color: #2d4373 !important;">{{App\Helpers\PriceHelper::stringPriceWithTwoDigits($order->totaalprijs)}}</td>

                    </tr>

                    <tr>
                        <td class="col-xs-3" style="color: #2d4373 !important;">BTW 21%</td>

                        <td class="col-xs-3"
                            style=" color: #2d4373 !important; padding-left: 35px;">{{App\Helpers\PriceHelper::stringPriceWithTwoDigits(($order->order_total_inc_btw) * 0.21)}}</td>

                    </tr>
                    <tr>
                        <td class="col-xs-3" style="color: #2d4373 !important;">Totaalprijs (incl BTW)</td>

                        <td class="col-xs-3"
                            style=" color: #2d4373 !important; padding-left: 35px;">{{App\Helpers\PriceHelper::stringPriceWithTwoDigits(($order->order_total_inc_btw))}}</td>

                    </tr>
                @else
                    <tr>
                        <td class="col-xs-3" style="color: #2d4373 !important;">Totaalprijs (excl BTW)</td>

                        <td class="col-xs-3"
                            style="padding-left: 35px; color: #2d4373 !important;">{{App\Helpers\PriceHelper::stringPriceWithTwoDigits($order->totaalprijs)}}</td>

                    </tr>
                @endif
                <tr>
                    <td colspan="2" style="height: 20px; color: #2d4373 !important;"></td>
                </tr>

                <tr>
                    <td class="col-xs-3" style="color: #2d4373 !important;">Korting</td>

                    <td class="col-xs-3" style="padding-left: 35px; color: #2d4373 !important;">
                        -{{App\Helpers\PriceHelper::stringPriceWithTwoDigits($order->korting)}}</td>

                </tr>
                <tr>
                    <td class="col-xs-3" style="color: #2d4373 !important;">Aanbetaling</td>

                    <td class="col-xs-3" style="padding-left: 35px; color: #2d4373 !important;">
                        -{{App\Helpers\PriceHelper::stringPriceWithTwoDigits($order->aanbetaling)}}</td>

                </tr>
                <tr>
                    <td colspan="2" style="height: 20px;"></td>
                </tr>
                <tr>
                    <td class="col-xs-3" style="color: #2d4373 !important;">Nog te betalen</td>
                    @if($isInclBTW)
                        <td class="col-xs-3"
                            style="padding-left: 35px; color: #2d4373 !important;">{{App\Helpers\PriceHelper::stringPriceWithTwoDigits($order->order_total_inc_btw - $order->aanbetaling)}}</td>
                    @else
                        <td class="col-xs-3"
                            style="padding-left: 35px; color: #2d4373 !important;">{{App\Helpers\PriceHelper::stringPriceWithTwoDigits($order->totaalprijs - $order->aanbetaling)}}</td>
                    @endif

                </tr>
                <tr>
                    <td class="col-xs-3" style="color: #2d4373 !important;">Betaalmethode</td>

                    <td class="col-xs-3" style="padding-left: 35px; color: #2d4373;">{{$order->betaalmethode}}</td>

                </tr>
            </table>


        </td>
    </tr>


    <tr>
        <td style="width: 100%; margin-top: 50px;">
            @if($isOfferte)
                <p style="color: #2d4373 !important; font-weight: bold ">
                    Nog vragen voor uw offerte?
                </p>
            @else
                <p class="col-md-12 margin" style="color: #2d4373 !important; font-weight: bold ">Nog vragen voor uw
                    bestelling?
                </p>
            @endif

        </td>
    </tr>
    <tr>
        <td>
            <p class="col-md-12 margin" style="font-size: small; color: #2d4373 !important;">Genemuiden:
                info@fabrieksverkoopgenemuiden.nl of 038-385 66 70</p>
            <p class="col-md-12 margin" style="font-size: small; color: #2d4373 !important;">Oudenbosch:
                oudenbosch@fabrieksverkoopgenemuiden.nl of 0165-79 4 95</p>
            <p style="font-size: small; color: #2d4373 !important; margin: 0; mso-line-height-rule:exactly;">Vragen
                over de
                bezorging? Neemt u
                contact op met planning@fabrieksverkoopgenemuiden.nl of 06 46 24 21 46</p>
        </td>
    </tr>
    <tr>
        <td style="width: 100%;">
            <p
                style=" font-size: small; font-weight: bold;  margin: 0; mso-line-height-rule:exactly; color: #2d4373 !important;">
                Fabrieksverkoop genemuiden &nbsp;&nbsp;&nbsp;&nbsp;info@fabrieksverkoopgenemuiden.nl&nbsp;&nbsp;&nbsp;&nbsp;038-385
                66 70</p>
            <p style="font-size: small; margin: 0; mso-line-height-rule:exactly; color: #2d4373 !important;">Team
                Fabrieksverkoop</p>
        </td>
    </tr>

</table>
</body>

</html>





