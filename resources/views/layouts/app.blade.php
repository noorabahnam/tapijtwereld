<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Fabrieksverkoop') }}</title>

    <!-- Styles -->
    <link href="{{ asset('/css/styles.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link href="{{ asset('/dist/css/AdminLTE.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/plugins/datatables.net-bs/css/dataTables.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/fonts/font-awesome-4.7.0/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/dist/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/dist/css/daterangepicker.css') }}" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js" rel="javascript"
            type="text/javascript"></script>
    <script src="https://cdn.polyfill.io/v2/polyfill.min.js?features=Intl.~locale.fr"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link href="https://fonts.googleapis.com/css?family=Signika+Negative&display=swap" rel="stylesheet">
</head>
<body
    style="background-color: #f1f3f9; margin: 0 !important; padding: 0 !important; width: 100% !important; font-family: 'Signika Negative', sans-serif !important; font-style: normal; font-weight: 400; font-size: 18px !important;">
<div id="app">

    @yield('content')


</div>


<!-- Scripts -->
<!-- Latest compiled and minified JavaScript -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js" rel="javascript"
        type="text/javascript"></script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
        integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
        crossorigin="anonymous"></script>
<script src="{{ asset('/dist/js/bootstrap-datepicker.min.js') }}" rel="javascript" type="text/javascript"></script>
<script src="{{ asset('/dist/js/bootstrap-datepicker.nl.min.js') }}" rel="javascript" type="text/javascript"></script>
<script src="{{ asset('/js/priceScripts.js') }}" rel="javascript" type="text/javascript"></script>
<script src="{{ asset('/dist/js/moment.min.js') }}" rel="javascript" type="text/javascript"></script>
<script src="{{ asset('/dist/js/daterangepicker.js') }}" rel="javascript" type="text/javascript"></script>
<script src="{{ asset('/plugins/datatables.net/js/jquery.dataTables.min.js') }}" rel="javascript"
        type="text/javascript"></script>
<script src="{{ asset('/plugins/datatables.net-bs/js/dataTables.bootstrap.min.js') }}" rel="javascript"
        type="text/javascript"></script>
@yield('js')
</body>

</html>
