@extends('layouts.app')
@section('css')

    {{--here css files....--}}

@stop
@section('content')
    {!! Form::open(array('files' => true)) !!}

    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-xs-12"
                     style="background-color: #252e65; width: 100%; font-family: canada-type-gibson, sans-serif; font-style: normal; font-weight: 400;">

                    <div class="col-xs-3 col-sm-1">
                        <a href="javascript:history.go(-1)" class="btn btn-block btn-default btn-md margin"
                           style="background-color: #252e65"><i class="fa fa-chevron-left" aria-hidden="true"
                                                                style="color: white"></i></a>
                    </div>
                    <div class="col-xs-6 col-sm-10 ">
                        @if($isOfferte)
                            <h6 class="col-xs-12"
                                style="color: white; font-family: canada-type-gibson, sans-serif; font-style: normal; font-weight: 700; display:flex; justify-content: center; align-items:center; margin-top: 10px">
                                NIEUWE OFFERTE</h6>
                        @else
                            <h6 class="col-xs-12"
                                style="color: white; font-family: canada-type-gibson, sans-serif; font-style: normal; font-weight: 700;; display:flex; justify-content: center; align-items:center; margin-top: 10px">
                                NIEUWE BESTELLING</h6>
                        @endif
                        <h6 class="col-xs-12"
                            style="color: #ffffff;display:flex; font-family: canada-type-gibson, sans-serif; font-style: normal; font-weight: 700; justify-content: center; align-items:center; margin-top: 10px">
                            Product toevoegen </h6>
                    </div>

                    <div class="col-xs-3 col-sm-1">
                        <a href="{{route("home")}}" class="btn btn-block btn-default btn-md margin"
                           style="background-color: #252e65;"><i class="fa fa-home" aria-hidden="true"
                                                                 style="color: white"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-12" style="display:flex; justify-content: center; align-items:center; margin-top: 10px">
        <img src="{{URL::asset('/uploads/' . $company .".png")}}" alt="" style=" height: 70px"/>
    </div>

    @if ($errors->any())
        <div class="alert alert-danger margin col-md-12">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="col-md-12">
        <div class="row margin">
            <h4 class="col-xs-12" style=" text-align: left; color:#2d4373;"><b>1. SELECTEER PRODUCTGROEP</b></h4>
            <div class="col-xs-3">
                <button type="button" class="P1 btn btn-block btn-default btn-lg margin"
                        style=" color:#2d4373; background-color: white;"
                        value="Tapijt" id="Tapijt">
                    <b>TAPIJT</b></button>
            </div>
            <div class="col-xs-3">
                <button type="button" class="P1 btn btn-block btn-default btn-lg margin"
                        style=" color:#2d4373; background-color: white;"
                        value="Laminaat"
                        id="Laminaat"><b>LAMINAAT</b></button>
            </div>
            <div class="col-xs-3">
                <button type="button" class="P1 btn btn-block btn-default btn-lg margin"
                        style=" color:#2d4373; background-color: white;" value="PVC" id="PVC">
                    <b>PVC</b></button>
            </div>
            <div class="col-xs-3">
                <button type="button" class="P1 btn btn-block btn-default btn-lg margin"
                        style="color:#2d4373; background-color: white;" value="Vinyl" id="Vinyl"><b>VINYL</b>
                </button>
            </div>
            <div class="col-xs-3">
                <button type="button" class="P1 btn btn-block btn-default btn-lg margin"
                        style="color:#2d4373; background-color: white;" value="Kunstgras"
                        id="Kunstgras"><b>KUNSTGRAS</b></button>
            </div>
            <div class="col-xs-3">
                <button type="button" class="P1 btn btn-block btn-default btn-lg margin"
                        style="color:#2d4373; background-color: white;" value="Sisal" id="Sisal"><b>SISALTAPIJT</b>
                </button>
            </div>
            <div class="col-xs-3">
                <button type="button" class="P1 btn btn-block btn-default btn-lg margin"
                        style="color:#2d4373; background-color: white;" value="Projecttapijt"
                        id="Projecttapijt"><b>PROJECTTAPIJT</b></button>
            </div>
            <div class="col-xs-3">
                <button type="button" class="P1 btn btn-block btn-default btn-lg margin"
                        style=" color:#2d4373; background-color: white;" value="Tapijttegels"
                        id="Tapijttegels"><b>TAPIJTTEGELS</b></button>
            </div>
            <div class="col-xs-3">
                <button type="button" class="P1 btn btn-block btn-default btn-lg margin"
                        style="color:#2d4373; background-color: white;" value="Beurstapijt"
                        id="Beurstapijt"><b>BEURSTAPIJT</b></button>
            </div>

        </div>
    </div>

    <input id="selectProduct" name="selectProduct" type="hidden" value="">
    <div id="ProductInfo" style="display: none;">
        <div class="col-md-12">
            <div class="row margin">
                <h4 class="col-xs-12" style="text-align: left;  color:#2d4373;"><b>2. PRODUCT SAMENSTELLEN</b></h4>
                <div class="col-xs-4">
                    <div class=" ui-widget">
                        <textarea type="text" name="message" id="message" class="form-control" required="required"
                                  placeholder="Omschrijving"
                                  style="height: 100px;">{{$product->description ?? old("message")}}</textarea>
                    </div>
                </div>
                <div class="col-xs-8">
                    <table style="margin-top: 10px;">
                        <tr style="height: 50px;">
                            <td class="col-xs-2">
                                <label
                                    style=" justify-content: center; text-align: center; color:#2d4373;">Prijs</label>
                            </td>
                            <td class="col-xs-4" colspan="1">
                                <input type="number" name="price"
                                       value="@if($product->product_price != null && $product->product_price != ""){{App\Helpers\PriceHelper::priceWithTwoDigits($product->product_price)}}@else{{ old("price")}}@endif"
                                       class="col-xs-6 @if($errors->has("price")) has-error @endif" id="ProductPrice"
                                       placeholder="Prijs" onchange="myFunction(event)" step="0.01"
                                       required="required"/>
                            </td>
                            <td class="col-xs-4" colspan="1">
                                <label class="switch" style="float:left;"><input type="checkbox" name="pricePreEenheid"
                                                                                 id="checkboxPrice"
                                                                                 onchange="myFunction(event)" @if($product->pricePerUnit == 1 || $product == null){{'checked'}}@endif><span
                                        value="checked" class="slider round"></span></td>
                            <td class="col-xs-6">
                                </label><span style="color:#2d4373;">Prijs per eenheid</span>
                            </td>
                        </tr>
                        <tr style="height: 50px;">
                            <td class="col-xs-2">
                                <label
                                    style=" justify-content: center; text-align: center; color:#2d4373;">Aantal</label>
                            </td>
                            <td class="col-xs-2" colspan="1">
                                <input type="number" name="count" value="{{$product->count ?? old("count")}}"
                                       class="col-xs-4 @if($errors->has("count")) has-error @endif" id="ProductCount"
                                       placeholder="Aantal" onkeyup="myFunction(event)" required="required"/>

                                <select autocomplete="off" id="eenheid" name="eenheid" class="col-xs-2 "
                                        style="color:#2d4373; border-style: none; text-align: center; width: 55px; height: 26px; padding: 0 !important;"
                                        required="required">
                                    @if(!in_array($product->product_type, ['Laminaat', 'PVC', 'Tapijttegels']))
                                        <option label="m1"
                                                value="m1" {{ $product->unit != null && $product->unit == "m1" ? "selected" :  old("eenheid") == "m1" ? "selected" : ""}}>
                                            m1
                                        </option>
                                    @endif
                                    <option label="m2"
                                            value="m2" {{$product->unit != null && $product->unit == "m2" ? "selected" :old("eenheid") == "m2" ? "selected" : ""}}>
                                        m2
                                    </option>
                                </select>

                            </td>
                            <td class="col-xs-4" colspan="1">
                                <label class="col-xs-2"
                                       style=" justify-content: center; text-align: center; color:#2d4373;">Totaalprijs</label>
                            </td>
                            <td class="col-xs-6">
                                <label id="product_totalprijs" name="product_totalprijs"
                                       style=" justify-content: center; text-align: center; color:#2d4373;"
                                       required="required">@if($product->product_price != null){{App\Helpers\PriceHelper::stringPriceWithTwoDigits($product->pricePerUnit == 1 ? $product->product_price * $product->count : $product->product_price)}} @else
                                        € 0,00 @endif</label>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="row margin">
                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-btn">
                            <span class="btn btn-default btn-file"
                                  style="color:#2d4373; background-color: white; border-color: #2d4373; margin-left: 30px">
                                <i class="fa fa-fw fa-camera-retro"></i> Voeg foto toe<input type="file" name="photo"
                                                                                             onchange="readURL(this);"
                                                                                             accept="image/png, image/jpeg, image/gif"
                                                                                             id="imgInp">
                            </span>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12 ">
            <div class="row margin">
                <div class="form-group">
                    <img id="img" src="@if($product->photo != null) URL::asset('/uploads/' . $product->photo @endif"
                         alt="" width="100" style="margin-left: 30px"/>
                </div>
            </div>
        </div>
        <div class="col-md-12" style="color:#2d4373;">
            <div class="row margin">
                <h4 class="col-xs-12" style="text-align: left;"><b>3. SNIJMATEN</b></h4>
                <table class="table table-bordered" style="color: #2d4373; border-collapse: unset" id="data">
                    <tbody>
                    </tbody>
                    <tfoot>
                    <tr>
                        <td>
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>
                            <table class=""
                                   style="border: 1px solid black; background-color: white; width: 200px;">
                                <tbody>
                                <tr style="border: 1px solid black;">
                                    <td style="padding: 5px;">Totaal</td>
                                    <td id="filled_area" style="padding: 5px;">0

                                    </td>
                                </tr>
                                <tr>
                                    <td id="rest_area_label" style="padding: 5px;">Resterend

                                    </td>
                                    <td id="rest_area" style="padding: 5px;">0

                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    </tfoot>
                </table>
            </div>

        </div>
        <div class="col-md-11">
            <div class="row margin">
                <div class="form-group">
                    <a id="add_row" class="btn btn-default"
                       style="color:#2d4373; background-color: white; border-color: #2d4373; float: right;"><i
                            class="fa fa-plus-circle" aria-hidden="true"></i> Voeg een snijmaat toe</a>
                </div>
            </div>
        </div>


        @if($product->product_type != 'Beurstapijt')
            <div class="col-md-12">
                <div class="row margin">
                    <h4 class="col-xs-12 " style=" text-align: left;  color:#2d4373;"><b>4. ACCESSOIRES</b></h4>
                    <table class="col-md-12" style="color: #2d4373; margin-top: 10px;">
                        <?php $count = 0;?>
                        @foreach($productInfo as $item => $value)
                            <tr>
                                <th class="col-xs-4">{{$item}} <input type="hidden" name="acc_name[]" value="{{$item}}">
                                </th>
                                <th class="col-xs-2">@if($value !== "" && $value !== 0) {{App\Helpers\PriceHelper::stringPriceWithTwoDigits($value)}}@endif @if($value === "")
                                        <input type="number" id="acc_price_{{{$count+1}}}" name="acc_price[]"
                                               placeholder="Prijs invoeren" onkeyup="myAccessoires(this)"
                                               data-inputid="aantal_count_{{{$count+1}}}" data-price="{{{$count+1}}}"
                                               value="@if($product->accessories != null && json_decode($product->accessories) != [] && count(json_decode($product->accessories)) > $count && json_decode($product->accessories)[$count][1] != 0){{json_decode($product->accessories)[$count][1]}}@endif"
                                               step="0.01"> @else <input type="hidden" value="{{$value}}"
                                                                         name="acc_price[]"/> @endif</th>
                                <th class="col-xs-2">@if($product->product_type == "Kunstgras")
                                        <?php
                                        switch ($count) {
                                            case 0:
                                            case 2:
                                                echo "per m2";
                                                break;
                                            case 1:
                                            case 3:
                                                echo "per stuk";
                                                break;
                                            case 4:
                                                echo "per tube";
                                                break;
                                        }

                                        ?>
                                    @else

                                        per m2
                                    @endif</th>
                                <th class="col-xs-2"><input type="number" step="0.01" id="aantal_count_{{$count+1}}"
                                                            name="acc_count[]" placeholder="Aantal" class="form-control"
                                                            onkeyup="myAccessoires(this)"
                                                            @if($value !== "") data-el="{{$value}}"
                                                            @else data-inputid="acc_price_{{{$count+1}}}"
                                                            @endif data-price="{{{$count+1}}}"
                                                            value="@if($product->accessories != null && json_decode($product->accessories) != [] &&  count(json_decode($product->accessories)) > $count && (json_decode($product->accessories))[$count][2] !== 0){{(json_decode($product->accessories))[$count][2]}}@endif">
                                </th>
                                <?php
                                $price = "€ 0,00";
                                $curP = ($product->accessories != null && json_decode($product->accessories) != [] && count(json_decode($product->accessories)) > $count) ? json_decode($product->accessories) : null;
                                if ($curP != null) {
                                    $temp = doubleval($curP[$count][1]) * doubleVal($curP[$count][2]);
                                    $price = App\Helpers\PriceHelper::stringPriceWithTwoDigits($temp);
                                }

                                ?>
                                <th class="col-xs-2 acc_price" id="TypeInfo{{$count+1}}">{{$price}}</th>


                            </tr>
                            <?php $count += 1?>
                        @endforeach
                    </table>
                </div>
            </div>
        @endif

        <div class="col-md-12">
            <div class="margin">
                <h4 class="col-xs-12" style="height: 50px; text-align: left;  color:#2d4373;"><b>5. PLINTEN</b></h4>
                <button type="button" class="P1 btn btn-default btn-lg margin "
                        style=" color:#2d4373; width: 18%; background-color: white;" id="PlintBerlijn">
                    <b>BERLIJN</b></button>

                <button type="button" class="P1 btn btn-default btn-lg margin"
                        style=" color:#2d4373; width: 18%; background-color: white;"
                        id="PlintAmsterdam"><b>AMSTERDAM</b></button>

                <button type="button" class="P1 btn btn-default btn-lg margin"
                        style=" color:#2d4373; width: 18%; background-color: white;" id="PlintPraag">
                    <b>PRAAG</b></button>

                <button type="button" class="P1 btn btn-default btn-lg margin"
                        style=" color:#2d4373; width: 18%; background-color: white;" id="PlintMilaan">
                    <b>MILAAN</b></button>
                <button type="button" class="P1 btn btn-default btn-lg margin-top margin-left"
                        style=" color:#2d4373; width: 18%; background-color: white;" id="PlintenPlak">
                    <b>PLAK</b></button>

                <input type="hidden" name="plintenType" id="plintenType"/>
                <input type="hidden" name="plintenPrice" id="plintenPriceInput"/>

            </div>
        </div>

        <div id="PlintenInfo" style="display: none;">
            <div class="col-md-12" style="color: #2d4373; ">
                <div class="row margin">
                    <div class="col-xs-12 margin">
                        <div id="AllPlinten">
                            <div class="col-xs-2 list-group">
                                <button type="button" class="list-group-item list-group-item-action"
                                        style=" background-color: #b9bbbe; color: #2d4373; text-align: center;"
                                        id="HoogWit"><B>HOOG WIT</B></button>
                                <button type="button" class="list-group-item list-group-item-action"
                                        style=" color:#2d4373; text-align: center;" id="HoogWit1">70 (€
                                    2,92)
                                </button>
                                <button type="button" class="list-group-item list-group-item-action"
                                        style=" color:#2d4373; text-align: center;" id="HoogWit2">90 (€
                                    3,75)
                                </button>
                                <button type="button" class="list-group-item list-group-item-action"
                                        style=" color:#2d4373; text-align: center;" id="HoogWit3">120 (€
                                    5,00)
                                </button>
                            </div>
                            <div class="col-xs-2 list-group">
                                <button type="button" class="list-group-item list-group-item-action"
                                        style=" background-color: #b9bbbe; color: #2d4373; text-align: center;"
                                        id="HoogInKleur"><B>HOOG IN KLEUR</B></button>
                                <button type="button" class="list-group-item list-group-item-action"
                                        style=" color: #2d4373; text-align: center;" id="HoogInKleur1">70
                                    (€ 3,75)
                                </button>
                                <button type="button" class="list-group-item list-group-item-action"
                                        style=" color: #2d4373; text-align: center;" id="HoogInKleur2">90
                                    (€ 5,00)
                                </button>
                                <button type="button" class="list-group-item list-group-item-action"
                                        style=" color: #2d4373; text-align: center;" id="HoogInKleur3">120
                                    (€ 6,25)
                                </button>
                            </div>
                        </div>
                        <div id="PlakPlinten" class="col-xs-2 list-group" style="display: none;">
                            <button type="button" class="list-group-item list-group-item-action"
                                    style="background-color: #b9bbbe; color: #2d4373; text-align: center;"
                                    id="Plakplint"><B>PLAKPLINT</B></button>
                            <button type="button" class="list-group-item list-group-item-action"
                                    style=" color: #2d4373; text-align: center;" id="Plakplint1">(€
                                2,00)
                            </button>
                        </div>

                        <div class="col-xs-3 list-group">
                            <label class="col-md-12 control-label"
                                   style=" justify-content: center; text-align: center; margin-top: 50px"
                                   id="PlintAantal">Aantal plinten</label>
                        </div>
                        <div class="col-xs-2 list-group">
                            <input type="number" name="plintenCount" style="margin-top: 50px;"
                                   value="{{$product->plintCount ?? old("plintenCount")}}"
                                   class="form-control @if($errors->has("count")) has-error @endif" id="Plintaantal"
                                   placeholder="Aantal" onkeyup="plintenAantal(event)"/>
                        </div>
                        <input type="hidden" name="PlintenInfoDetails" id="PlintenInfoDetails"/>
                    </div>
                </div>
            </div>
            <div class="col-md-12" style="color: #2d4373; ">
                <div class="row ">
                    <div class="col-xs-12">
                        <div class="col-xs-6" style="color:#2d4373;"></div>
                        <div class="col-xs-3">
                            <label class="col-md-12 control-label"
                                   style=" justify-content: center; text-align: center;"
                                   id="plintPrice">Totaalprijs</label>
                        </div>
                        <div class="col-xs-2">
                            <label class="col-md-12 control-label" id="PlintenPrice"
                                   style=" justify-content: center; text-align: center;">@if($product->plintCount)
                                    App\Helpers\PriceHelper::stringPriceWithTwoDigits($product->plintenPrice *
                                    $product->plintCount) @else €0,00 @endif</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12" style="color: #2d4373;">
            <div class="row margin">
                <h4 class="col-xs-12" style="height: 50px; text-align: left;  color:#2d4373;"><b>6. LEGSERVICE</b></h4>
                <div class="col-xs-3" style="color:#2d4373;">
                    <span class="col-xs-3">Nee</span>
                    <label class="switch" style="float: left"><input type="checkbox" name="legserviceChecked"
                                                                     id="legService"
                                                                     onchange="checkboxed()" @if($product->legService == 1){{'checked'}}@endif><span
                            class="slider round"></span></label>
                    <span class="col-xs-3">ja</span>
                </div>

                <div class="col-xs-3">
                    <input type="number" name="legservicePrice" class="form-control" step="0.01" id="legservicePrice"
                           placeholder="Prijs" onkeyup="myFunction1(event)"
                           @if($product->legService == 1)style="display : block;" value="{{$product->legServicePrice}}"
                           @else style="display : none;" value="" @endif/>
                </div>
                <div class="col-xs-3">
                    <label class="col-md-12 control-label"
                           style=" justify-content: center; text-align: center; margin-top: 10px;"
                           id="servicetotalPrice">Totaalprijs</label>
                </div>
                <div class="col-xs-2">
                    <label id="legserviceLabel" name="legservice" class="col-md-12 control-label"
                           style=" justify-content: center; text-align: center; margin-top: 10px;"></label>
                </div>
            </div>
        </div>
        @if($isInclBTW)
            <div class="col-md-12">
                <div class="row margin"
                     style="background-color: #4d5bae; border-radius: 15px; color: white; height: 100px;">
                    <table class="col-md-12" style=" margin-top: 20px">
                        <tr>
                            <th class="col-xs-2">
                                <h4 class="col-xs-12">Totaalprijs</h4>
                                <h4 class=" col-xs-12" style=" margin-top: -5px;">Exc. BTW</h4>
                            </th>
                            <th class="col-xs-2" id="priceLabelEx" name="product_prijs"></th>
                            <th class="col-xs-2">
                                <h4 class="col-xs-12">BTW</h4>
                                <h4 class="col-xs-12" style=" margin-top: -5px;">21%</h4>
                            </th>
                            <th class="col-xs-2" id="priceLabelBTW" name="product_prijs"></th>
                            <th class="col-xs-2">
                                <h4 class="col-xs-12">Totaalprijs</h4>
                                <h4 class="col-xs-12" style=" margin-top: -5px;">incl. BTW</h4>
                            </th>
                            <th class="col-xs-2" id="priceLabel" name="product_prijs"></th>
                            <input id="inputPriceProduct" name="product_prijs" type="hidden" value="">
                            <input id="inputBTWProduct" name="product_btw" type="hidden" value="">
                            <input id="inputTotalIncProduct" name="product_total_inc_btw" type="hidden" value="">
                        </tr>
                    </table>
                </div>
            </div>
        @else
            <div class="col-md-12">
                <div class="row margin"
                     style="background-color: #4d5bae; border-radius: 15px; color: white; line-height: 40px; text-align: center">
                    <div class="col-xs-8">
                        <h6 class="col-xs-12"
                        >Totaalprijs</h6>
                        <h6 class="col-xs-12" style=" margin-top: -5px;">Exc. BTW </h6>
                    </div>
                    <div class="col-xs-2">
                        <div class="form-group">
                            <label id="priceLabelEx" class="form-group"></label>
                            <input id="inputPriceProduct" name="product_prijs" type="hidden" value="">
                        </div>
                    </div>
                </div>

            </div>
        @endif

    </div>
    <div class="col-md-12">
        <div class="row margin">
            <input id="" type="submit" value="TOEVOEGEN" class="btn btn-block btn-default"
                   style="background-color: #e66505;; color: white; border-radius: 10px; font-size: 18px;"/>
        </div>
    </div>
    {!! Form::close() !!}

    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <script type="text/javascript">

        //auto complete
        var autoFills = <?php echo $autoFill ?>;
        console.log(autoFills);
        $('#message').autocomplete({
            source: autoFills,
        });

        var ProductType = null;
        var temp = "<?php echo $product->product_type; ?>";
        var selectProduct = (temp !== "") ? temp : null;
        var isIncl = <?php echo $isInclBTW; ?>;

        proClick(selectProduct, false);

        $('#Kunstgras').click(function () {
            proClick($(this).attr('id'), true)
        });
        $('#Laminaat').click(function () {
            proClick($(this).attr('id'), true)
        });
        $('#PVC').click(function () {
            proClick($(this).attr('id'), true)
        });
        $('#Tapijt').click(function () {
            proClick($(this).attr('id'), true)
        });
        $('#Vinyl').click(function () {
            proClick($(this).attr('id'), true)
        });
        $('#Sisal').click(function () {
            proClick($(this).attr('id'), true)
        });
        $('#Beurstapijt').click(function () {
            proClick($(this).attr('id'), true)
        });
        $('#Projecttapijt').click(function () {
            proClick($(this).attr('id'), true)
        });
        $('#Tapijttegels').click(function () {
            proClick($(this).attr('id'), true)
        });

        function proClick(id, post) {

            if (selectProduct !== "" && selectProduct != null) {
                var el = $("#" + selectProduct);
                el.css("background-color", "white");
                el.css("color", "#2d4373");
            }

            if (post) {
                window.location.href = "/product/" + id;
            }

            selectProduct = id;

            var el = $("#" + selectProduct);
            el.css("background-color", "#2d4373");
            el.css("color", "white");
            $("#selectProduct").val(selectProduct);

            if (id !== "" && id != null) {
                var ProductInfo = document.getElementById('ProductInfo');
                ProductInfo.style.display = "block";

            } else {
                var ProductInfo = document.getElementById('ProductInfo');
                ProductInfo.style.display = "none";
            }
        }

        function myAccessoires(input) {

            var item = input.dataset.el;
            var inputField = input.dataset.inputid;
            var priceVal = input.dataset.price;

            if (item !== undefined) {
                var itemPrice = item * input.value;
                document.getElementById("TypeInfo" + priceVal).innerHTML = "€ " + priceWithComma(priceWithTwoDigits(itemPrice));
            }

            if (inputField !== undefined) {
                var vv = document.getElementById(inputField);
                var itemPrice = vv.value * input.value;
                document.getElementById("TypeInfo" + priceVal).innerHTML = "€ " + priceWithComma(priceWithTwoDigits(itemPrice));
            }

            calculateTotalPrice();
        }


        //plinten
        var t = '<?php echo $product->plintenInfo; ?>';
        var selectPlinten = t !== "" && t !== null && t !== undefined ? "<?php echo $product->plintenInfo; ?>" : null;
        var t1 = '<?php echo $product->plintenDetails; ?>';
        var selectedPlintenType = t1 !== "" && t1 !== null && t1 !== undefined ? "<?php echo $product->plintenDetails; ?>" : null;
        var selectedPlintenPrice = "<?php echo $product->plintenPrice; ?>";

        $('#GeenPlinten').click(function () {
            handleClick($(this).attr('id'))
        });
        $('#PlintBerlijn').click(function () {
            handleClick($(this).attr('id'))
        });
        $('#PlintAmsterdam').click(function () {
            handleClick($(this).attr('id'))
        });
        $('#PlintPraag').click(function () {
            handleClick($(this).attr('id'))
        });
        $('#PlintMilaan').click(function () {
            handleClick($(this).attr('id'))
        });

        $('#PlintenPlak').click(function () {
            handleClick($(this).attr('id'))
        });

        $('#HoogWit1').click(function () {
            typeClick($(this).attr('id'))
        });
        $('#HoogWit2').click(function () {
            typeClick($(this).attr('id'))
        });
        $('#HoogWit3').click(function () {
            typeClick($(this).attr('id'))
        });
        $('#HoogInKleur1').click(function () {
            typeClick($(this).attr('id'))
        });
        $('#HoogInKleur2').click(function () {
            typeClick($(this).attr('id'))
        });
        $('#HoogInKleur3').click(function () {
            typeClick($(this).attr('id'))
        });
        $('#Plakplint1').click(function () {
            typeClick($(this).attr('id'))
        });


        handleClick(selectPlinten);
        typeClick(selectedPlintenType);

        $('#add_row').click(function () {
            add(event)
        });

        function handleClick(id) {

            if (selectPlinten !== "" && selectPlinten !== null) {
                var el = $("#" + selectPlinten);
                el.css("background-color", "white");
                el.css("color", "#2d4373");
            }

            selectPlinten = id;

            var el = $("#" + selectPlinten);
            el.css("background-color", "#4d5bae");
            el.css("color", "white");
            $("#selectPlinten").val(selectPlinten);

            if (id !== "GeenPlinten" && id !== null) {
                var PlintenInfo = document.getElementById('PlintenInfo');
                var AllPlinten = document.getElementById('AllPlinten');
                var PlakPlinten = document.getElementById('PlakPlinten');
                PlintenInfo.style.display = "block";
                AllPlinten.style.display = id == "PlintenPlak" ? "none" : "block";
                PlakPlinten.style.display = id != "PlintenPlak" ? "none" : "block";

                document.getElementById("plintenType").value = selectPlinten;
                if (id === "PlintenPlak") {
                    typeClick("Plakplint1");
                }


            } else {
                var PlintenInfo = document.getElementById('PlintenInfo');
                PlintenInfo.style.display = "none";
                document.getElementById("plintenType").value = "";
            }


        }

        function typeClick(id) {

            if (selectedPlintenType !== "") {
                var el = $("#" + selectedPlintenType);
                el.css("background-color", "white");
                el.css("color", "#2d4373");
            }

            selectedPlintenType = id;

            var el = $("#" + selectedPlintenType);
            el.css("background-color", "#4d5bae");
            el.css("color", "white");
            $("#selectedPlintenType").val(selectedPlintenType);


            if (id === "HoogWit1") selectedPlintenPrice = 2.92;
            if (id === "HoogWit2") selectedPlintenPrice = 3.75;
            if (id === "HoogWit3") selectedPlintenPrice = 5.00;
            if (id === "HoogInKleur1") selectedPlintenPrice = 3.75;
            if (id === "HoogInKleur2") selectedPlintenPrice = 5.00;
            if (id === "HoogInKleur3") selectedPlintenPrice = 6.25;
            if (id === "Plakplint1") selectedPlintenPrice = 2.00;

            document.getElementById("PlintenInfoDetails").value = selectedPlintenType;
            document.getElementById("plintenPriceInput").value = selectedPlintenPrice;
            plintenAantal(null);
        }

        function plintenAantal(event) {
            var aantal = document.getElementById("Plintaantal").value;

            var totalPlintPrice = aantal * selectedPlintenPrice;

            totalPlintPrice = priceWithComma(priceWithTwoDigits(totalPlintPrice));

            document.getElementById("PlintenPrice").innerHTML = "€ " + totalPlintPrice;

            calculateTotalPrice();
        }

        // upload photo
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#img').attr('src', e.target.result);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }

        //add snijmaten
        var currentSMIndex = 0;

        var current = <?php if ($product->cutMeasures != null) echo $product->cutMeasures; else echo 'undefined';?>;
        if (current === null || current === undefined || current === "")
            currnet = [];

        var list = "";
        if (current !== null && current !== undefined && current !== "" && current.length > 0) {
            for (var i = 0; i < current.length; i++) {
                currentSMIndex = i;
                list += "<tr><td>" + (i + 1) + "</td>" + "<td>" + "<input type='text' name='snijmatenruimte[]' value='" + current[i][0] + "'/>" + "</td>" + "<td>" + "<input class='smw_change' data-id='" + i + "' type='number' step='0.01' name='snijmatenwidth[]' value='" + current[i][1] + "'/>" + "</td>" + "<td>" + "<input class='sml_change' data-id='" + i + "' type='number' step='0.01' name='snijmatenlength[]' value='" + current[i][2] + "'/>" + "</td>" + "<td data-area='" + (Number(current[i][1] / 100) * (current[i][2] / 100)).toFixed(2) + "' class='area_result' id='result_sm'" + i + "'>= " + (Number(current[i][1] / 100) * (current[i][2] / 100)).toFixed(2) + "m2</td></tr>"
            }
            // document.getElementById('data').innerHTML = list;
            $("#data > tbody").append(list);

            addEventsToSM();
            calcAreaTotal();

        } else {
            add();
        }


        function add(event) {
            if (event)
                event.preventDefault();
            var list = "";
            list += "<tr><td>" + (currentSMIndex + 1) + "</td>" + "<td>" + "<input type='text' name='snijmatenruimte[]' value='' placeholder='Naam ruimte'/>" + "</td>" + "<td>" + "<input class='smw_change' data-id='" + currentSMIndex + "' type='number' name='snijmatenwidth[]' value='' step='0.01' placeholder='Lengte in cm'/>" + "</td>" + "<td>" + "<input class='sml_change'  step='0.01' data-id='" + currentSMIndex + "' type='number' name='snijmatenlength[]' value='' id='Length' placeholder='Breedte in cm'/>" + "</td>" + "<td  data-area='0' class='area_result' id='result_sm'" + currentSMIndex + "'></td></tr>";
            $("#data > tbody").append(list);
            currentSMIndex += 1;
            addEventsToSM();
            return false;
        }

        function addEventsToSM() {
            $('.smw_change').each(function () {
                $(this).keyup(function () {
                    var length = $("input", $(this).parent().next()).val();
                    var res = calcArea($(this).val(), length);
                    $($(this).parent().next().next().html("= " + res + "m2"));
                    $(this).parent().next().next().attr('data-area', res);
                    calcAreaTotal();
                });
            });

            $('.sml_change').each(function () {
                $(this).keyup(function () {
                    var width = $("input", $(this).parent().prev()).val();
                    var res = calcArea(width, $(this).val());
                    $($(this).parent().next().html("= " + res + "m2"));
                    $(this).parent().next().attr('data-area', res);
                    calcAreaTotal();
                });
            });
        }

        function calcArea(w, h) {
            var res = (Number(w / 100) * (h / 100)).toFixed(2);
            return isNaN(res) ? 0 : res;
        }

        function checkboxed() {
            var check = document.getElementById("legService").checked;
            if (check) {
                $("#legservicePrice").prop('disabled', false);
                $("#legservicePrice").prop('required', 'required');

                $("#legservicePrice").show();
            } else {
                $("#legservicePrice").prop('disabled', true);
                $("#legservicePrice").prop('required', '');

                $("#legservicePrice").hide();
            }
        }

        function calcAreaTotal() {
            totalArea = 0;
            $('.area_result').each(function () {

                var p = $(this).attr('data-area');
                totalArea += Number(p);
                console.log($(this).attr('data-area'));

            });

            $('#filled_area').html(totalArea.toFixed(2));

            var countVAl = $('#ProductCount').val();

            var rest = Number(countVAl - totalArea).toFixed(2);

            $('#rest_area').html(rest);
            if (rest < 0) {
                $('#rest_area').attr("style", 'color: red; padding:5px;');
                $('#rest_area_label').attr("style", 'color: red; padding:5px;');
                $('#rest_area_label').html('Te kort');
            } else {
                $('#rest_area').attr("style", 'color: #2d4373; padding:5px;');
                $('#rest_area_label').attr("style", 'color: #2d4373; padding:5px;');
                $('#rest_area_label').html('Resterend');
            }
        }

        function myFunction(event) {
            var aantal = document.getElementById("ProductCount").value;
            if (aantal.indexOf(",") !== -1)
                aantal = aantal.replace(",", ".");
            aantal = parseFloat(aantal);
            var price = document.getElementById("ProductPrice").value;
            var checkprice = document.getElementById("checkboxPrice").checked;

            if (price.indexOf(",") !== -1)
                price = price.replace(",", ".");
            price = parseFloat(price);

            var finalPrice = price;


            if (checkprice) {
                finalPrice = (aantal * price);
            }

            document.getElementById("product_totalprijs").innerHTML = "€ " + priceWithComma(priceWithTwoDigits(finalPrice));
            calculateTotalPrice();
            calcAreaTotal();
        }

        function myFunction1(event) {
            var optionsRadios = document.getElementById("legService").checked;
            var legserviceChecked = document.getElementById('legservicePrice');
            document.getElementById("legserviceLabel").innerHTML = "€ " + priceWithComma(priceWithTwoDigits(legserviceChecked.value));
            calculateTotalPrice();
        }

        function calculateAccPrice() {
            var accPrice = 0;
            //acc elements
            $(".acc_price").each(function (index) {
                var val = $(this).text();
                val = val.replace("€ ", "");
                val = val.replace(",", ".");
                var floatVal = parseFloat(val);
                console.log(floatVal);
                if (!isNaN(floatVal) && floatVal !== undefined && floatVal !== "")
                    accPrice += floatVal;
            });
            if (!isNaN(accPrice) && accPrice !== undefined && accPrice !== "")
                return accPrice;

            return 0;
        }

        function calculatePlintenPrice() {
            var plintenPrice = $("#PlintenPrice").text();
            plintenPrice = plintenPrice.replace("€ ", "");
            plintenPrice = plintenPrice.replace(",", ".");
            var floatVal = parseFloat(plintenPrice);
            if (!isNaN(floatVal) && floatVal !== undefined && floatVal !== "")
                return floatVal;

            return 0;
        }

        function calculateProductPrice() {
            var pPrice = $("#ProductPrice").val();
            var pCount = $("#ProductCount").val();
            var checkprice = document.getElementById("checkboxPrice").checked;

            if (checkprice)
                pPrice = pPrice * pCount;

            if (!isNaN(pPrice) && pPrice !== undefined && pPrice !== "")
                return pPrice;

            return 0;
        }

        function calculateLegServicePrice() {
            var legService = $("#legservicePrice").val();
            if (!isNaN(legService) && legService !== undefined && legService !== "")
                return legService;
            return 0;
        }


        function calculateTotalPrice() {
            var accPrice = calculateAccPrice();
            var plintenPrice = calculatePlintenPrice();
            var ProductPrice = calculateProductPrice();
            var legService = calculateLegServicePrice();

            var tPrice = parseFloat(accPrice) + parseFloat(plintenPrice) + parseFloat(ProductPrice) + parseFloat(legService);
            var BTW = (tPrice / 1.21) * 0.21;
            var tIncBTW = parseFloat(tPrice) - parseFloat(BTW);
            var totalPriceEx = document.getElementById('priceLabelEx');

            //check if inclusive BTW
            if (isIncl) {
                var totalPriceInc = document.getElementById('priceLabel');
                var totalBTW = document.getElementById('priceLabelBTW');

                totalPriceInc.innerHTML = "€ " + priceWithComma(priceWithTwoDigits(tPrice));
                totalBTW.innerHTML = "€ " + priceWithComma(priceWithTwoDigits(BTW));
                totalPriceEx.innerHTML = "€ " + priceWithComma(priceWithTwoDigits(tIncBTW));

                document.getElementById("inputBTWProduct").value = BTW;
                document.getElementById("inputTotalIncProduct").value = tIncBTW;

            } else {
                totalPriceEx.innerHTML = "€ " + priceWithComma(priceWithTwoDigits(tPrice));

            }

            document.getElementById("inputPriceProduct").value = tPrice;
        }

        function priceWithComma(price) {
            var stringPrice = "" + price;
            if (stringPrice.indexOf(".") !== -1)
                stringPrice = stringPrice.replace(".", ",");

            return stringPrice;
        }

        function priceWithTwoDigits(price) {
            var numberPrice = Number(price);
            return numberPrice.toFixed(2);
        }

    </script>

@stop

