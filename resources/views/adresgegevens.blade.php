@extends('layouts.app')
@section('css')

    {{--here css files....--}}
@stop
@section('content')
    {{--{{dd($order)}}--}}
    {!! Form::open() !!}
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-xs-12" style="background-color: #252e65; width: 100%">
                    <div class="col-xs-3 col-sm-1">
                        <a href="{{route("productoverzicht")}}" class="btn btn-block btn-default btn-md margin"
                           style="background-color: #252e65"><i class="fa fa-chevron-left" aria-hidden="true"
                                                                style="color: white"></i></a>
                    </div>
                    <div class="col-xs-6 col-sm-10">

                        <h5 class="col-xs-12 margin"
                            style="color: white; font-weight: bold; line-height: 40px; display:flex; justify-content: center; align-items:center">
                            Adresgegevens</h5>
                    </div>
                    <div class="col-xs-3 col-sm-1">
                        <a href="{{route("home")}}" class="btn btn-block btn-default btn-md margin"
                           style="background-color: #252e65"><i class="fa fa-home" aria-hidden="true"
                                                                style="color: white"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12"
         style="display:flex; justify-content: center; align-items:center; margin-top: 10px; margin-bottom: 15px">
        <img src="{{URL::asset('/uploads/' . $company .".png")}}" alt="" style=" height: 70px"/>
    </div>

    <div class="">
        <div class="form-group margin">
            <div class="col-md-2">
                <select id="subject" name="Aanhef" class="form-control" required="required">
                    <option value="heer">dhr</option>
                    <option value="mevrouw">mevr</option>
                    <option value="bedrijf">bedrijf</option>
                </select>
            </div>
            <div class="col-md-10">
                {{Form::input('hidden', 'order_id', isset($order->id)? $order->id : null)}}
                {{Form::input('text', 'name',  $order->naam ?? session('old')['name'] , ['class' =>  ($errors->has("name")) ? 'form-control has-error' : 'form-control', 'placeholder' => 'Naam', 'id' => '', ''])}}
            </div>
        </div>
    </div>


    <div class="col-md-12">
        <div class="form-group margin">
            {{Form::input('email', 'email', $order->email ?? session('old')['email'], ['class' =>  ($errors->has("email")) ? 'form-control has-error' : 'form-control', 'placeholder' => 'E-mail', 'id' => '', ''])}}
        </div>
    </div>


    <div class="col-md-12">
        <div class="form-group margin">
            {{Form::input('tel', 'tel', $order->tel ?? session('old')['tel'], ['class' =>  ($errors->has("tel")) ? 'form-control has-error' : 'form-control', 'placeholder' => 'Telefoon', 'id' => '', ''])}}
        </div>
    </div>


    <div class="">
        <div class="form-group margin">
            <div class="col-md-8">
                {{Form::input('text', 'postcode', $order->postcode ?? session('old')['postcode'], ['class' =>  ($errors->has("postcode")) ? 'form-control has-error' : 'form-control', 'placeholder' => 'Postcode bijv. 1234AB', 'id' => '', /*'required', 'pattern' => '^[1-9][0-9]{3}(?!sa|sd|ss|SA|SD|SS)[A-Za-z]{2}$' */])}}
            </div>

            <div class="col-md-4">
                {{Form::input('text', 'hn', $order->huisnummer ?? session('old')['hn'], ['class' =>  ($errors->has("hn")) ? 'form-control has-error' : 'form-control', 'placeholder' => 'Huisnummer', 'id' => '', ''])}}
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="form-group margin">
            {{Form::input('text', 'sn', $order->straatnaam ?? session('old')['sn'], ['class' =>  ($errors->has("sn")) ? 'form-control has-error' : 'form-control', 'placeholder' => 'Straatnaam', 'id' => '', ''])}}
        </div>
    </div>

    <div class="">
        <div class="form-group margin">
            <div class="col-md-8">
                {{Form::input('text', 'city', $order->plaats ?? session('old')['city'], ['class' =>  ($errors->has("city")) ? 'form-control has-error' : 'form-control', 'placeholder' => 'Plaats', 'id' => '', ''])}}
            </div>
            <div class="col-md-4">
                {{Form::input('text', 'country', $order->country ?? (session('old')['country'] ?? "NL"), ['class' =>  ($errors->has("country")) ? 'form-control has-error' : 'form-control', 'placeholder' => 'NL', 'id' => '', 'required'])}}
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="row" style="margin-top: 10px;">
            <div class="form-group">
                <div class="col-xs-9">
                    <label class="form-control"
                           style="border-style: none; background-color: transparent; color: #2d4373;">Heb je andere
                        afleveradres?</label>
                </div>
                <div class="col-xs-3">
                    <label class="switch"><input type="checkbox" name="afleverChecked" id="aflever"
                                                 onchange="checkboxed1()"><span class="slider round"></span></label>
                </div>
            </div>
        </div>
    </div>
    <div id="afleveradres" style="display: none;">
        <div class="">
            <div class="form-group margin">
                <div class="col-md-8">
                    {{Form::input('text', 'postcodeaflever', $order->postcodeaflever ?? session('old')['postcodeaflever'], ['class' =>  ($errors->has("postcodeaflever")) ? 'form-control has-error' : 'form-control', 'placeholder' => 'Postcode', 'id' => ''])}}
                </div>

                <div class="col-md-4">
                    {{Form::input('text', 'hnaflever', $order->huisnummeraflever ?? session('old')['hnaflever'], ['class' =>  ($errors->has("hn")) ? 'form-control has-error' : 'form-control', 'placeholder' => 'Huisnummer', 'id' => ''])}}
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="form-group margin">
                {{Form::input('text', 'snaflever', $order->straatnaamaflever ?? session('old')['snaflever'], ['class' =>  ($errors->has("sn")) ? 'form-control has-error' : 'form-control', 'placeholder' => 'Straatnaam', 'id' => ''])}}
            </div>
        </div>

        <div class="">
            <div class="form-group margin">
                <div class="col-md-8">
                    {{Form::input('text', 'cityaflever', $order->plaatsaflever ?? session('old')['cityaflever'], ['class' =>  ($errors->has("city")) ? 'form-control has-error' : 'form-control', 'placeholder' => 'Plaats', 'id' => ''])}}
                </div>
                <div class="col-md-4">
                    {{Form::input('text', 'countryaflever', $order->countryaflever ?? (session('old')['countryaflever'] ?? "NL") , ['class' =>  ($errors->has("country")) ? 'form-control has-error' : 'form-control', 'placeholder' => 'NL', 'id' => ''])}}
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="row margin">
            <input type="submit" value="VERDER" class="btn btn-block btn-default"
                   style="background-color:#e66505; color: white; border-radius: 10px"/>
        </div>
    </div>
    {!! Form::close() !!}
@stop

@section("js")
    <script>

        function checkboxed1() {
            var afleverChecked = document.getElementById('aflever');
            var postcodeaflever = document.getElementById('afleveradres');

            if (afleverChecked.checked === true) {
                postcodeaflever.style.display = "block";
            } else {
                postcodeaflever.style.display = "none";
            }
        }

    </script>

@stop
