@extends('layouts.app')
@section('css')

    {{--here css files....--}}
@stop
@section('content')
    {!! Form::open() !!}

    @if(!$isPDF)
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-xs-12" style="background-color: #252e65; width: 100%">
                        <div class="col-xs-9 col-sm-11"></div>
                        <div class="col-xs-3 col-sm-1">
                            <a href="{{route("home")}}" class="btn btn-block btn-default btn-md margin"
                               style="background-color: #252e65"><i class="fa fa-home" aria-hidden="true"
                                                                    style="color: white"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <div class="col-md-12 ">
        <div class="row" style="margin-top: 10px">
            <div class="col-xs-12">
                <div class="col-xs-8 no-padding">

                    @if($isOfferte)
                        <h4 class="col-xs-12" style="color: #2d4373; font-weight: bold;">BEDANKT VOOR JE OFFERTE!</h4>
                    @else
                        <h4 class="col-xs-12" style="color: #2d4373; font-weight: bold;">BEDANKT VOOR JE
                            BESTELLING!</h4>
                    @endif
                </div>
                <div class="col-xs-4">
                    <img src="{{URL::asset('/uploads/' . $company .".png")}}" alt=""
                         style=" height: 70px; float: right"/>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-12 ">
        <div class="row margin">
            <div class="col-xs-12">
                <div class="col-xs-6 no-padding">
                    <h4 class="col-xs-12 no-padding" style="font-weight: bold; color: #2d4373">Beste
                        &nbsp;{{$order->naam}},</h4>
                    <div class="col-xs-12 no-padding">
                        @if($isOfferte)
                            <h4 class=" no-padding"
                                style="border-style: none; font-size: small; background-color: transparent; color: #2d4373">
                                Je
                                offerte is opgeslagen.</h4>
                        @else
                            <h4 class=" no-padding"
                                style="border-style: none; background-color: transparent; color: #2d4373">
                                Je
                                bestelling met ordernummer {{$order->id}} is verwerkt. Je factuur vind je in de bijlage
                                van deze mail.</h4>
                        @endif
                    </div>
                </div>
                <div class="col-xs-6" style="color: #2d4373">
                    <h4 class="col-md-12">Bestelnummer:&nbsp;&nbsp;<b>{{$order->id}}</b></h4>
                    <h4 class="col-md-12" style="margin-top: -5px">Datum:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>{{date('d-m-Y', strtotime($order->updated_at))}}</b>
                    </h4>
                    <h4 class="col-md-12" style="margin-top: -5px">
                        Verkoper:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>{{$employee}}</b></h4>
                </div>
            </div>
        </div>
    </div>


    <div class="col-md-12">
        <div class="row margin">
            <table class="col-xs-12 margin" style="color:#2d4373;border: #2d4373 1px solid;">
                <tr style="font-weight: bold; font-size: medium">
                    <th class="col-xs-6" colspan="2">Uw gegevens</th>

                    <th class="col-xs-6" colspan="2">Aflevergegevens</th>

                </tr>

                <tr>
                    <td class="col-xs-2">Naam:</td>
                    <td class="col-xs-4">{{$order->naam}}</td>
                    <td class="col-xs-3">Naam:</td>
                    <td class="col-xs-3">{{$order->naam}}</td>
                </tr>
                <tr>
                    <td class="col-xs-2">Adres:</td>
                    <td class="col-xs-4">{{$order->straatnaam . ' ' . $order->huisnummer}}</td>
                    <td class="col-xs-3">Adres:</td>
                    <td class="col-xs-3">@if($order->straatnaamaflever != null && $order->straatnaamaflever != "") {{$order->straatnaamaflever . ' ' . $order->huisnummeraflever }} @else {{$order->straatnaam . ' ' . $order->huisnummer}} @endif </td>
                </tr>
                <tr>
                    <td class="col-xs-2">Postcode:</td>
                    <td class="col-xs-4">{{$order->postcode}}</td>
                    <td class="col-xs-3">Postcode:</td>
                    <td class="col-xs-3">@if($order->postcodeaflever != null && $order->postcodeaflever != "") {{$order->postcodeaflever }} @else {{$order->postcode}} @endif </td>

                </tr>
                <tr>
                    <td class="col-xs-2">Plaats:</td>
                    <td class="col-xs-4">{{$order->plaats. '-' . $order->country}}</td>
                    <td class="col-xs-3">Plaats:</td>
                    <td class="col-xs-3">@if($order->plaatsaflever != null && $order->plaatsaflever != "") {{$order->plaatsaflever . "-" . $order->countryaflever}} @else {{$order->plaats . '-' . $order->country}} @endif </td>

                </tr>
                <tr>
                    <td class="col-xs-2">Telefoon:</td>
                    <td class="col-xs-4">{{$order->tel}}</td>
                    <td class="col-xs-2">Telefoon:</td>
                    <td class="col-xs-4">{{$order->tel}}</td>
                </tr>
                <tr>
                    <td class="col-xs-2">E-mail:</td>
                    <td class="col-xs-4">{{$order->email}}</td>
                    <td class="col-xs-2">E-mail:</td>
                    <td class="col-xs-4">{{$order->email}}</td>
                </tr>
            </table>
        </div>
    </div>



    <div class="col-md-12">
        <div class="row margin">
            @if($isOfferte)
                <h4 class="col-md-12 margin"
                    style="color: #2d4373; font-weight: bold; text-align: left; margin-top: 20px; background-color: transparent;">
                    Overzicht van uw offerte!</h4>
            @else
                <h4 class="col-md-12 margin"
                    style="color: #2d4373; font-weight: bold; text-align: left; margin-top: 20px; background-color: transparent;">
                    Overzicht van uw bestelling</h4>
            @endif
        </div>
    </div>

    @foreach($products as $key => $product)

        <div class="col-md-12" id="product_{{$product->id}}">
            <div class="row margin" style="background-color: white; border-radius: 15px;">
                <table class="col-md-12">
                    <tr>
                        <td class="col-xs-2">
                            @if($product->photo != null && $product->photo != "")
                                <img src="{{URL::asset('/uploads/' . $product->photo)}}" alt="" width="60"
                                     style="border-radius: 50%; margin-top: 15px;"/>
                            @endif
                        </td>

                        <td class="col-xs-3">
                            <h4 class="col-xs-12"
                                style="color: #2d4373; font-weight: bold">{{$product->product_type}}</h4>
                            <h4 class="col-xs-12" style="color: #2d4373; margin: 0; ">{{$product->description}}</h4>
                            <h4 class="col-xs-12" style="color: #2d4373; font-weight: 700;">Snijmaten</h4>
                            @if($product->cutMeasures != null && count(json_decode($product->cutMeasures)) > 0)
                                @foreach (json_decode($product->cutMeasures) as $arr)
                                    <h4 class="col-xs-12"
                                        style="color: #2d4373; margin: 0; ">
                                        - {{$arr[0] . ': ' . $arr[1] .' x '. $arr[2] .'cm '}}</h4>
                                @endforeach
                            @endif
                        </td>
                        <td class="col-xs-3">
                            <h4 class="col-xs-12"
                                style="color: #2d4373;">{{$product->count . ' ' . $product->unit}}</h4>
                        </td>

                        <td class="col-xs-3">
                            <div class="form-group">
                                <label class="form-control"
                                       style="border-style: none; color: #2d4373; background-color: white; font-size: 18px;">{{App\Helpers\PriceHelper::stringPriceWithTwoDigits($product->pricePerUnit == 1 ? ($product->product_price * $product->count) : $product->product_price)}}</label>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td colspan="5">
                            <?php foreach (json_decode($product->accessories) as $arr) {
                            if (($arr[2] == 0)) continue; ?>

                            <table class="col-xs-12" style="margin-top: 5px;">
                                <tr class="col-xs-2"></tr>
                                <tr class="col-xs-4">
                                    <td class="col-xs-12"
                                        style="color: #2d4373; margin: 0; font-size: 18px; ">{{$arr[0]}}</td>
                                </tr>
                                <tr class="col-xs-3">
                                    <td class="col-xs-12"
                                        style="color: #2d4373; margin: 0; font-size: 18px;"> {{App\Helpers\PriceHelper::stringPriceWithTwoDigits($arr[1]) . ' x ' . $arr[2]}}</td>
                                </tr>
                                <tr class="col-xs-3">
                                    <td class="form-group">
                                        <label class="form-control"
                                               style="border-style: none; color: #2d4373; font-size: 18px;">{{App\Helpers\PriceHelper::stringPriceWithTwoDigits($arr[1] * $arr[2])}}</label>
                                    </td>
                                </tr>

                            </table>
                            <?php } ?>
                        </td>
                    </tr>

                    @if($product->plintenInfo != null && $product->plintenInfo !== "Geenplinten")
                        <tr>
                            <td class="col-xs-2"></td>

                            <td class="col-xs-4">
                                <h4 class="col-xs-12" style="color: #2d4373;">{{$product->plintenInfo}}</h4>
                                <h4 class="col-xs-12"
                                    style="color: #2d4373; margin: 0; ">{{$product->plintenDetails}}</h4>
                                <h4 class="col-xs-12"
                                    style="color: #2d4373;  ">{{'€ '. $product->plintenPrice . ' per meter '}}</h4>
                            </td>
                            <td class="col-xs-3">
                                <h4 class="col-xs-12" style="color: #2d4373;">{{$product->plintCount . ' m1 '}}</h4>
                            </td>

                            <td class="col-xs-3">
                                <div class="form-group">
                                    <label class="form-control"
                                           style="border-style: none; color: #2d4373; background-color: white; font-size: 18px;">{{App\Helpers\PriceHelper::stringPriceWithTwoDigits($product->plintenPrice * $product->plintCount)}}</label>
                                </div>
                            </td>
                        </tr>
                    @endif
                    <tr>
                        <td class="col-xs-2"></td>

                        <td class="col-xs-4">
                            <h4 class="col-xs-12" style="color: #2d4373; margin: 0; ">Legservice</h4>
                        </td>
                        <td class="col-xs-3">
                            <h4 class="col-xs-12"
                                style="color: #2d4373;">{{$product->legService == 0 ? "Nee" : "Ja"}} </h4>
                        </td>

                        <td class="col-xs-3">
                            <div class="form-group">
                                <label class="form-control"
                                       style="border-style: none; color: #2d4373; background-color: white; font-size: 18px;">{{$product->legService == 0 ? '' : App\Helpers\PriceHelper::stringPriceWithTwoDigits($product->legServicePrice)}}</label>
                            </div>
                        </td>
                    </tr>

                </table>
            </div>
        </div>

    @endforeach


    @if($order->bezorgen == 1)
        <div class="col-md-12" id="product_{{$product->id}}">
            <div class="row margin" style="background-color: white; border-radius: 15px;">
                <table class="col-md-12">
                    <tr>
                        <td class="col-xs-2">
                            <img class="img-responsive" src="{{URL::asset('/uploads/Bezorgservice.png')}}" alt=""
                                 width="50" style=" margin-top: 10px"/>
                        </td>

                        <td class="col-xs-3">
                            <h4 class="col-xs-12" style="color: #2d4373; font-weight: bold">Bezorging</h4>
                        </td>
                        <td class="col-xs-4">
                            <h4 class="col-md-12 margin" style="color: #2d4373;">Afleverdatum:
                                tussen {{$order->bezorgdatumBegin}} - {{$order->bezorgdatumEnd}}</h4>
                        </td>
                        <td class="col-xs-3">
                            <label class="col-md-12" style="border-style: none; color: #2d4373;">&#8364
                                50,00</label>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    @endif



    <div class="col-xs-12" style="font-size:18px">
        <div class="row margin"
             style="border-style: none; background-color:#4d5bae; color: white; border-radius: 15px;">
            <div class="col-xs-12">
                <div class="col-xs-6">
                    <h4 class="col-md-12" style="font-weight: bold ">Belangrijk voor bezorging / legging</h4>
                    <ul style="list-style-position: inside;">
                        <li>De vloer dient leeg te zijn</li>
                        <li>Vloer is vlak en schoon</li>
                        <li>Fabriekverkoop is niet aansprakelijk voor schade aan meubels/voorwerpen tijdens bezorging /
                            leggen
                        </li>
                        <li>Betalling voldaan zijn voor aflevering</li>
                    </ul>
                    <h6 class="col-md-6" style="font-size: 18px ">Meer informatie op
                        <a href="https://fabrieksverkoopgenemuiden.nl/bezorging"
                           style="color: white !important; " target="_blank"><u>https://fabrieksverkoopgenemuiden.nl/bezorging</u></a>
                    </h6>
                </div>

                <div class="col-xs-6" style="margin-top: 10px; margin-bottom: 10px">
                    <table class="col-xs-12">
                        @if($isInclBTW)
                            <tr>
                                <td class="col-xs-3">Totaalprijs (excl BTW)</td>

                                <td class="col-xs-3"
                                    style="padding-left: 35px;">{{App\Helpers\PriceHelper::stringPriceWithTwoDigits($order->totaalprijs)}}</td>

                            </tr>

                            <tr>
                                <td class="col-xs-3">BTW 21%</td>

                                <td class="col-xs-3"
                                    style="padding-left: 35px;">{{App\Helpers\PriceHelper::stringPriceWithTwoDigits(($order->order_total_inc_btw/1.21) * 0.21)}}</td>

                            </tr>
                            <tr>
                                <td class="col-xs-3">Totaalprijs (incl BTW)</td>

                                <td class="col-xs-3"
                                    style="padding-left: 35px;">{{App\Helpers\PriceHelper::stringPriceWithTwoDigits(($order->order_total_inc_btw))}}</td>

                            </tr>
                        @else
                            <tr>
                                <td class="col-xs-3">Totaalprijs (excl BTW)</td>

                                <td class="col-xs-3"
                                    style="padding-left: 35px;">{{App\Helpers\PriceHelper::stringPriceWithTwoDigits($order->totaalprijs)}}</td>

                            </tr>
                        @endif
                        <tr>
                            <td colspan="2" style="height: 20px;"></td>
                        </tr>

                        <tr>
                            <td class="col-xs-3">Korting</td>

                            <td class="col-xs-3" style="padding-left: 35px;">
                                -{{App\Helpers\PriceHelper::stringPriceWithTwoDigits($order->korting)}}</td>

                        </tr>
                        <tr>
                            <td class="col-xs-3">Aanbetaling</td>

                            <td class="col-xs-3" style="padding-left: 35px;">
                                -{{App\Helpers\PriceHelper::stringPriceWithTwoDigits($order->aanbetaling)}}</td>

                        </tr>
                        <tr>
                            <td colspan="2" style="height: 20px;"></td>
                        </tr>
                        <tr>
                            <td class="col-xs-3">Nog te betalen</td>
                            @if($isInclBTW)
                                <td class="col-xs-3"
                                    style="padding-left: 35px;">{{App\Helpers\PriceHelper::stringPriceWithTwoDigits($order->order_total_inc_btw - $order->aanbetaling)}}</td>
                            @else
                                <td class="col-xs-3"
                                    style="padding-left: 35px;">{{App\Helpers\PriceHelper::stringPriceWithTwoDigits($order->totaalprijs - $order->aanbetaling)}}</td>
                            @endif

                        </tr>
                        <tr>
                            <td class="col-xs-3">Betaalmethode</td>

                            <td class="col-xs-3" style="padding-left: 35px;">{{$order->betaalmethode}}</td>

                        </tr>
                    </table>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="row margin">
                @if($isOfferte)
                    <h4 class="col-md-12 margin" style="color: #2d4373; font-weight: bold ">Nog vragen voor je
                        offerte?</h4>
                @else
                    <h4 class="col-md-12 margin" style="color: #2d4373; font-weight: bold ">Nog vragen voor je
                        bestelling?</h4>
                @endif
                <h4 class="col-md-12 margin" style="color: #2d4373;">Genemuiden:
                    <a style="color: #2d4373;"
                       href="mailto:info@fabrieksverkoopgenemuiden.nl"><u>info@fabrieksverkoopgenemuiden.nl</u></a>
                    of <a style="color: #2d4373;"
                          href="tel:0383856670"><u>038-3856670</u></a>
                </h4>
                <h4 class="col-md-12 margin" style=" color: #2d4373;">Oudenbosch: <a style="color: #2d4373;"
                                                                                     href="mailto:oudenbosch@fabrieksverkoopgenemuiden.nl"><u>oudenbosch@fabrieksverkoopgenemuiden.nl</u></a>
                    of <a style="color: #2d4373;"
                          href="tel:016579495"><u>0165-79495</u></a>
                </h4>
                <h4 class="col-md-12 margin" style="color: #2d4373;">Vragen over je bezorging? Neem contact op met
                    <a style="color: #2d4373;"
                       href="mailto:planning@fabrieksverkoopgenemuiden.nl"><u>planning@fabrieksverkoopgenemuiden.nl</u>
                    </a>
                    of <a style="color: #2d4373;"
                          href="tel:0646242146"><u>0646242146</u></a>
                </h4>
            </div>
        </div>

        <div class="col-md-12">
            <div class="row margin"
                 style="border-style: none; background-color:#4d5bae; color: white; border-radius: 15px;">
                <h4 class="col-md-12 margin" style=" font-weight: bold;  margin-top: 20px "> Fabrieksverkoop genemuiden&nbsp;&nbsp;
                    <a style="color: white !important;"
                       href="mailto:info@fabrieksverkoopgenemuiden.nl"><u>info@fabrieksverkoopgenemuiden.nl</u>
                    </a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <a style="color: white !important;"
                       href="tel:0383856670"><u>0383856670</u>
                    </a>
                </h4>
                <h4 class="col-md-12 margin">Team Fabrieksverkoop</h4>
            </div>
        </div>
        @if(!$isPDF)
            <div class="col-md-12">
                <div class="row margin">
                    <a href="{{ route('download-pdf',['id'=>$order->id]) }}" class="btn btn-block btn-default"
                       style="background-color:#e66505; color: white; border-radius: 10px; font-size: 18px;">Pakbon
                        afdrukken <i
                            class="fa fa-chevron-right" aria-hidden="true"
                            style="color: white;  border-radius: 10px"></i></a>
                </div>
            </div>
        @endif
    </div>

    {!! Form::close() !!}


    <script>

    </script>

@stop
