@extends('layouts.app')
@section('css')

    {{--here css files....--}}
@stop
@section('content')
    {!! Form::open() !!}

    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-xs-12" style="background-color: #252e65; width: 100%">
                    <div class="col-xs-3 col-sm-1">
                        <a href="{{route("home")}}" class="btn btn-block btn-default btn-md margin" style="background-color: #252e65"><i class="fa fa-chevron-left" aria-hidden="true" style="color: white"></i></a>
                    </div>
                    <div class="col-xs-6 col-sm-10">
                        <h5 class="col-xs-12 margin" style="color: white; font-weight: bold; line-height: 40px; display:flex; justify-content: center; align-items:center; margin-top: 10px">Bestelling wijzigen</h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
        <br>
    <h4 class="col-xs-12" style="text-align: center; color:#2d4373;"><b>Bestelling zoeken</b></h4>

        <div class="col-md-12">
            <div class ="row margin">
                <h6 class="col-xs-12" style="color: #2d4373"><b>Zoeken op postcode</b></h6>
            </div>
        </div>

        <div class="col-md-12">
            <div class="row">
                <div class="col-xs-12">
                    <div class="col-xs-8" style="color:#2d4373">
                        <div class="form-group margin">
                            <input type="text" name="postcode" class="form-control" id=""placeholder="Postcode" />
                        </div>
                    </div>

                    <div class="col-xs-4">
                        <div class="form-group margin">
                            <input type="text" name="huisnummer" class="form-control" style="font-size:xx-small" id=""placeholder="Huisnummer" />
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="row margin">
                <input type="submit" value="Zoeken" class="btn btn-block btn-default" style="background-color:#e66505; color: white;  border-radius: 10px" />
            </div>
        </div>

        <hr class="md-col-12 margin">
        <div class="col-md-12">
            <div class ="row margin">
                <h6 class="col-xs-12" style="color: #2d4373"><b>Zoeken op Bestelnummer or Naam</b></h6>
            </div>
        </div>

        <div class="col-md-12">
            <div class="row">
                <div class="col-xs-12">
                    <div class="col-xs-8" style="color:#2d4373">
                        <div class="form-group margin">
                            <input type="text" name="order_id" class="form-control" id=""placeholder="Bestelnummer / Naam" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="row margin">
                <input type="submit" value="Zoeken" class="btn btn-block btn-default" style="background-color:#e66505; color: white;  border-radius: 10px" />
            </div>
        </div>
    {!! Form::close() !!}

@stop