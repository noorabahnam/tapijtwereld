@extends('layouts.app')
@section('css')

    {{--here css files....--}}
@stop
@section('content')

    @if ($errors->any())
        <div class="alert alert-danger margin">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    {!! Form::open(["id" => "begin_form"]) !!}
    <div class="col-md-12">
        <div class="row margin">
            <div class="col-xs-12">
                <div class="col-xs-3">
                    <button type="button" class="P1 btn btn-block btn-default btn-lg margin"
                            style=" background-color: white !important;" id="KVG"><img class="img-responsive"
                                                                                       src="{{URL::asset('/uploads/KVG.png')}}"
                                                                                       style="display: block; margin-left: auto; margin-right:auto; height: 100px"/>
                    </button>
                </div>

                <div class="col-xs-3">
                    <button type="button" class="P1 btn btn-block btn-default btn-lg margin"
                            style="background-color: white;" id="KV"><img class="img-responsive"
                                                                          src="{{URL::asset('/uploads/KV.png')}}"
                                                                          style="display: block; height: 100px; margin-left: auto; margin-right:auto;"/>
                    </button>
                </div>
                <div class=" col-xs-3">
                    <button type="button" class="P1 btn btn-block btn-default btn-lg margin"
                            style="background-color: white;" id="KJK"><img class="img-responsive"
                                                                           src="{{URL::asset('/uploads/KJK.png')}}"
                                                                           style=" display: block; height: 100px;  margin-left: auto; margin-right:auto"/>
                    </button>
                </div>
                <div class="col-xs-3">
                    <button type="button" class="P1 btn btn-block btn-default btn-lg margin"
                            style="background-color: white;" id="TAPIJT-INT"><img class="img-responsive"
                                                                                  src="{{URL::asset('/uploads/TAPIJT-INT.png')}}"
                                                                                  style=" display: block; height: 100px;  margin-left: auto; margin-right:auto;"/>
                    </button>
                </div>
            </div>
        </div>
    </div>




    <h4 class="col-xs-12" style="text-align: center; color:#2d4373;"><b>Medewerker</b></h4>



    <div class="col-md-12">
        <div class="row margin">
            <div class="col-xs-12">
                <div class="col-xs-3">
                    <button type="button" class="P1 btn btn-block btn-default btn-lg margin"
                            style="font-family: canada-type-gibson, sans-serif; font-style: normal; font-weight: 400; height: 100%; background-color: white; color:#2d4373;"
                            id="Lambert"><b>Lambert</b></button>
                </div>
                <div class="col-xs-3">
                    <button type="button" class="P1 btn btn-block btn-default btn-lg margin"
                            style="font-family: canada-type-gibson, sans-serif; font-style: normal; font-weight: 400; height: 100%; background-color: white; color:#2d4373;"
                            id="Johan">
                        <b>Johan</b></button>
                </div>
                <div class="col-xs-3">
                    <button type="button" class="P1 btn btn-block btn-default btn-lg margin"
                            style="font-family: canada-type-gibson, sans-serif; font-style: normal; font-weight: 400; height: 100%; background-color: white; color:#2d4373;"
                            id="Andre">
                        <b>Andre</b></button>
                </div>
                <div class="col-xs-3">
                    <button type="button" class="P1 btn btn-block btn-default btn-lg margin"
                            style="font-family: canada-type-gibson, sans-serif; font-style: normal; font-weight: 400; height: 100%; background-color: white; color:#2d4373;"
                            id="Jan"><b>Jan</b>
                    </button>
                </div>
                <div class="col-xs-3">
                    <button type="button" class="P1 btn btn-block btn-default btn-lg margin"
                            style="font-family: canada-type-gibson, sans-serif; font-style: normal; font-weight: 400; height: 100%; background-color: white; color:#2d4373;"
                            id="Robin">
                        <b>Robin</b></button>
                </div>
                <div class="col-xs-3">
                    <button type="button" class="P1 btn btn-block btn-default btn-lg margin"
                            style="font-family: canada-type-gibson, sans-serif; font-style: normal; font-weight: 400; height: 100%; background-color: white; color:#2d4373;"
                            id="Flex" ,
                            onclick="checkFlex(event)"><b>Flex</b></button>
                </div>
                <div class="col-xs-3">
                    {{Form::input('text', 'name', isset($order->naam) && $order->naam !=="" ? $order->naam : old("name"), ['class' =>  ($errors->has("name")) ? 'form-control has-error' : 'form-control', 'placeholder' => 'Naam', 'id' => 'EmployeeName',"style" => "display:none;font-family: canada-type-gibson, sans-serif; font-style: normal; font-weight: 400; height: 40px; margin-top: 13px; border-color: #4D5BAE; color: #2d4373;"])}}
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="row margin">
            <div class="col-xs-12">
                <div class="form-group">
                    <input type="submit" value="Nieuwe Bestelling excl BTW"
                           style="background-color: white; color:#2d4373; font-family: canada-type-gibson, sans-serif; font-style: normal; font-weight: 700;"
                           class="P1 btn btn-block btn-default margin" onclick="checkisexclBTW(event)"/>
                </div>
                <div class="form-group">
                    <input type="submit" value="Nieuwe Bestelling incl BTW"
                           style="background-color: white; color:#2d4373; font-family: canada-type-gibson, sans-serif; font-style: normal; font-weight: 700;"
                           class="P1 btn btn-block btn-default margin" onclick="checkisincBTW(event)"/>
                </div>

                <div class="form-group">
                    <button type="button" value="isOfferte"
                            style="background-color: white; color:#2d4373; font-family: canada-type-gibson, sans-serif; font-style: normal; font-weight: 700;"
                            class="P1 btn btn-block btn-default margin" id="Nieuwe offerte"
                            onclick="checkisofferte(event)">Nieuwe offerte
                    </button>
                </div>
                <div class="form-group">
                    <a href="/bestellingzoeken"
                       style="background-color: white; color:#2d4373; font-family: canada-type-gibson, sans-serif; font-style: normal; font-weight: 700;"
                       class=" P1 btn btn-block btn-default margin">Bestelling wijzigen</a>
                </div>

                <div class="form-group">
                    <a href="/overzichtspagina"
                       style="background-color: white; color:#2d4373; font-family: canada-type-gibson, sans-serif; font-style: normal; font-weight: 700;"
                       class="P1 btn btn-block btn-default margin">Overzichtspagina</a>
                </div>
                <div class="form-group">
                    <a href="/logout"
                       style="background-color: white; color:#2d4373; font-family: canada-type-gibson, sans-serif; font-style: normal; font-weight: 700;"
                       class="P1 btn btn-block btn-default margin">Uitloggen</a>
                </div>
            </div>
        </div>
    </div>

    <input id="selectCompany" name="selectCompany" type="hidden" value="">
    <input id="selectedEmployee" name="selectedEmployee" type="hidden" value="">
    <input id="isOfferte" name="isOfferte" type="hidden" value="0">
    <input id="isIncBTW" name="isIncBTW" type="hidden" value="0">

    {!! Form::close() !!}
    <script type="text/javascript">

        var selectCompany = null;
        var selectedEmployee = null;

        $('#KVG').click(function () {
            handleClick($(this).attr('id'))
        });
        $('#KV').click(function () {
            handleClick($(this).attr('id'))
        });
        $('#KJK').click(function () {
            handleClick($(this).attr('id'))
        });
        $('#TAPIJT-INT').click(function () {
            handleClick($(this).attr('id'))
        });

        $('#Lambert').click(function () {
            emplClick($(this).attr('id'))
        });
        $('#Johan').click(function () {
            emplClick($(this).attr('id'))
        });
        $('#Andre').click(function () {
            emplClick($(this).attr('id'))
        });
        $('#Jan').click(function () {
            emplClick($(this).attr('id'))
        });
        $('#Robin').click(function () {
            emplClick($(this).attr('id'))
        });
        $('#Flex').click(function () {
            emplClick($(this).attr('id'))
        });


        function handleClick(id) {

            if (selectCompany != "") {
                var el = $("#" + selectCompany);
                el.css("background-color", "white");
            }

            selectCompany = id;

            var el = $("#" + selectCompany);
            el.css("background-color", "#4d5bae");
            $("#selectCompany").val(selectCompany);
        }

        function emplClick(id) {
            if (selectedEmployee != "") {
                var name = $("#" + selectedEmployee);
                name.css("background-color", "white");
                name.css("color", "#000");
            }
            selectedEmployee = id;

            if (id === "Flex") {
                var EmployeeName = document.getElementById('EmployeeName');
                EmployeeName.style.display = "block";

            } else {
                var EmployeeName = document.getElementById('EmployeeName');
                EmployeeName.style.display = "none";
            }
            var name = $("#" + selectedEmployee);
            name.css("background-color", "#4d5bae");
            name.css("color", "#fff");
            $("#selectedEmployee").val(selectedEmployee);
        }

        function checkisofferte(event) {
            event.preventDefault();
            var offerte = $("#isOfferte");
            $("#isOfferte").val(1);

            $("#begin_form").submit();
        }

        function checkisincBTW(event) {
            event.preventDefault();
            var offerte = $("#isIncBTW");
            $("#isIncBTW").val(1);

            $("#begin_form").submit();
        }

        function checkisexclBTW(event) {
            event.preventDefault();
            var offerte = $("#isExclBTW");
            $("#isExclBTW").val(0);

            $("#begin_form").submit();
        }

        function checkFlex() {
            if (selectedEmployee === "Flex") {
                var EmployeeName = document.getElementById('EmployeeName');
                $("#selectedEmployee").val(EmployeeName.value);
            }
        }


    </script>

@stop
