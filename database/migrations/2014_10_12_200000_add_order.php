<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOrder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('naam')->nullable();
            $table->string('email')->nullable();
            $table->string('tel')->nullable();
            $table->string('postcode')->nullable();
            $table->string('huisnummer')->nullable();
            $table->string('straatnaam')->nullable();
            $table->string('plaats')->nullable();
            $table->string('country')->nullable();
            $table->boolean('bezorgen')->default(false);
            $table->string('bezorgdatumBegin')->nullable();
            $table->string('bezorgdatumEnd')->nullable();
            $table->string('postcodeaflever')->nullable();
            $table->string('huisnummeraflever')->nullable();
            $table->string('straatnaamaflever')->nullable();
            $table->string('plaatsaflever')->nullable();
            $table->string('countryaflever')->nullable();
            $table->string('betaalmethode')->nullable();
            $table->double('korting')->nullable();
            $table->double('aanbetaling')->nullable();
            $table->double('totaalprijs')->nullable();
            $table->string('handtekenen')->nullable();
            $table->string('bedrijfsnaam')->nullable();
            $table->string('medewekernaam')->nullable();

            $table->boolean('isOfferte');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
