<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create("products", function (Blueprint $table)
        {
            $table->increments('id');
            $table->timestamps();
            $table->string("product_type");
            $table->longText("description")->nullable();
            $table->double("product_price");
            $table->integer("count");
            $table->boolean("pricePerUnit");
            $table->string("unit");
            $table->double("productTotalPrice");
            $table->double("productTotalIncPrice")->nullable();
            $table->double("productBTW")->nullable();
            $table->longText("accessories")->nullable();
            $table->longText("cutMeasures")->nullable();
            $table->string("plintenInfo")->nullable();
            $table->integer("plintCount")->nullable();
            $table->double("plintenPrice")->nullable();
            $table->string("plintenDetails")->nullable();
            $table->boolean("legService");
            $table->double("legServicePrice")->nullable();
            $table->text("photo")->nullable();
            $table->integer('order_id')->unsigned();;
            $table->foreign('order_id')
                ->references('id')->on('orders')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('products');

    }
}
