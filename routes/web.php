<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect("/home");
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::post('/home', 'HomeController@postindex')->name('homepost');


Route::get('/product/{product?}', 'ProductController@AddProduct')->name('AddProduct');
Route::get('/product/edit/{id}', 'ProductController@editProduct')->name('editProduct');
Route::post('/product/edit/{id}', 'ProductController@postEditProduct');
Route::post('/product/', 'ProductController@postAddProductError');
Route::post('/product/{product}', 'ProductController@postAddProduct');

Route::get('/productoverzicht/{id?}', 'HomeController@ProductOverzicht')->name('productoverzicht');
Route::post('/productoverzicht/{id?}', 'HomeController@PostProductOverzicht');

Route::get('/product/delete/{id}', 'ProductController@deleteProduct')->name('deleteProduct');//Productoverzicht/deletproduct
Route::get('/product/copy/{id}', 'ProductController@copyProduct')->name('cpoyProduct');//Productoverzicht/deletproduct
///
///

Route::get('/adresgegevens', 'HomeController@BestelerGegevens')->name('adresgegevens');
Route::post('/adresgegevens', 'HomeController@postBestelerGegevens');

Route::get('/Ondertekenen', 'HomeController@Ondertekenen')->name('ondertekenen');
Route::post('/Ondertekenen', 'HomeController@postOndertekenen');

Route::get('/FinalOverzicht', 'HomeController@FinalOverzicht')->name('FinalOverzicht');
Route::post('/FinalOverzicht', 'HomeController@postFinalOverzicht');

Route::get('/PDFOverzicht', 'HomeController@PDFOverzicht')->name('pdf');

Route::get("download-pdf","HomeController@downloadPDF")->name('download-pdf');
Route::get("download-pdf/{id}","HomeController@downloadPDFForOrder")->name('download-pdf');

Route::get("/overzichtspagina", "HomeController@Overzichtspagina")->name('overzichtspagina');
Route::post('/overzichtspagina', 'HomeController@postOverzichtspagina');

//besteling wijzigen
Route::get('/bestellingzoeken', 'HomeController@BestellingWijzigen')->name('besttelingwijzigen');
Route::post('/bestellingzoeken', 'HomeController@postBestellingWijzigen');

Route::get('/zoekresultaten', 'HomeController@Zoekresultaten')->name('zoeken');
Route::post('/zoekresultaten', 'HomeController@postZoekresultaten');
//

Route::get("/logout", "HomeController@Logout");
