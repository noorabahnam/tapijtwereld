<?php

namespace App\Helpers;

/**
 * Created by PhpStorm.
 * User: wisambaraz
 * Date: 27/07/2018
 * Time: 10:41
 */


abstract class PriceHelper
{

    public static function stringPriceWithTwoDigits($price, $currency = "€")
    {
        $p = str_replace('.', ',', sprintf($currency . " %1\$.2f" , $price));
        return $p;
    }

    public static function priceWithTwoDigits($price)
    {
        $p = number_format((float)$price, 2, '.', "" );
        return $p;
    }

}
