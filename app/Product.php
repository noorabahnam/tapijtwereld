<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{

//    protected $casts = ["accessories" => 'array', "cutMeasures" => 'array'];
    protected $fillable =
        [
            'product_type',
            'description',
            'product_price',
            'count',
            'pricePerUnit',
            'unit',
            'productTotalPrice',
            'productTotalIncPrice',
            'productBTW',
            'accessories',
            'cutMeasures',
            'plintenInfo',
            'plintCount',
            'plintenPrice',
            'plintenDetails',
            'legService',
            'legServicePrice',
            'photo'
        ];

    protected $attributes = [
        'pricePerUnit' => 1
    ];

    public static function getProductTypeInfo($product)
    {
        switch ($product) {
            case "Kunstgras":
            {
                $collection = collect([
                    'Legservice' => "",
                    'Grashaken' => 1.00,
                    'Worteldoek' => 2.00,
                    'Lijmband' => 0.00,
                    'Lijm' => 14.95,
                ]);
                return $collection;
            }
            case "Laminaat":
            {
                $collection = collect([
                    'Legservice' => 10.00,
                    'Ondervloer standaard' => 0.00,
                    'Ondervloer Groene plaat' => 3.00,
                    'Ondervloer Rubber' => 4.50,
                ]);
                return $collection;
            }
            case "PVC":
            {
                $collection = collect([
                    'Legservice' => 20.00,
                    'Legservice visgraat' => "",
                    'Legservice visgraat met bies' => "",
                    'Egaline' => 10.00,
                ]);
                return $collection;
            }
            case "Tapijt":
            {
                $collection = collect([
                    'Legservice' => 5.00,
                    'Ondervloer Groene plaat' => 5.00,
                    'Ondervloer Rubber' => 5.00,
                ]);
                return $collection;
            }
            case "Vinyl":
            {
                $collection = collect([
                    'Legservice' => 5.00,
                ]);
                return $collection;
            }
            case "Sisal":
            {
                $collection = collect([
                    'Legservice' => 5.00,
                    'Ondervloer Groene plaat' => 5.00,
                ]);
                return $collection;
            }
            case "Beurstapijt":
            {
                $collection = collect([
                ]);
                return $collection;
            }
            case "Projecttapijt":
            {
                $collection = collect([
                    'Legservice' => 5.00,
                    'Ondervloer Groene plaat' => 5.00,
                    'Ondervloer Rubber' => 5.00,
                ]);
                return $collection;
            }

            case "Tapijttegels":
            {
                $collection = collect([
                    'Legservice' => 5.00,
                ]);
                return $collection;
            }

        }
    }
}
