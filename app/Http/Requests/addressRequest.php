<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class addressRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required',
            'tel' => 'required',
            'hn' => 'required',
            'sn' => 'required',
            'city' => 'required',
            'postcode' => 'required|regex:#^[1-9][0-9]{3} ?(?!sa|sd|ss)[a-z]{2}$/i#',
            'postcodeaflever' => 'nullable|regex:#^[[1-9][0-9]{3} ?(?!sa|sd|ss)[a-z]{2}$/i#'
        ];
    }
}
