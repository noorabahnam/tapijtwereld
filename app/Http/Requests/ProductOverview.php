<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductOverview extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            //
            'totaalprijs' => 'required',
            'optionsRadios' => 'required',
            'datepicker' => 'required_if:optionsRadios, option2',
            'paymentType' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'paymentType.required' => 'Selecteer betaal methode...'
        ];
    }
}
