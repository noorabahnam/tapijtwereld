<?php

namespace App\Http\Controllers;

use App\AutoFill;
use App\Http\Requests\addProduct;
use App\Order;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use PhpParser\Node\Expr\AssignOp\Div;

class ProductController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware('auth');
        setlocale(LC_MONETARY, 'nl_NL');
    }

    public function deleteProduct($id)
    {
        if ($id == 0)
            return "false";

        Product::where('id', $id)->delete();
        $this->deleteFromSession($id);
        return "true";
    }

    function deleteFromSession($id)
    {
        $products = session('products') ?? [];

        $productsTouched = [];
        foreach ($products as $pr) {
            if ($pr->id != $id) {
                $productsTouched[] = $pr;
            }
        }

        session(['products' => $productsTouched]);
        session()->save();
    }

    public function AddProduct($selectedProduct = null)
    {
        $isOfferte = session('isOfferte');
        session(['isOfferte' => $isOfferte]);

        $isIncBTW = session('isIncBTW');
        session(['isIncBTW' => $isIncBTW]);

        $product = new Product();
        $product->product_type = $selectedProduct;

        $productInfo = Product::getProductTypeInfo($selectedProduct) ?? [];

        $autofills = AutoFill::all();
        $autoFillStrings = [];
        foreach ($autofills as $autofill)
            $autoFillStrings[] = $autofill->text;
        $company = session('selectCompany') ?? "";
        return view('addproduct', ["company" => $company, 'isOfferte' => $isOfferte, 'isInclBTW' => $isIncBTW, 'product' => $product, 'productInfo' => $productInfo, 'autoFill' => json_encode($autoFillStrings)]);
    }

    public function editProduct($id)
    {
        $isOfferte = session('isOfferte');
        session(['isOfferte' => $isOfferte]);

        $isIncBTW = session('isIncBTW');
        session(['isIncBTW' => $isIncBTW]);

        $product = Product::findOrFail($id);

        $productInfo = Product::getProductTypeInfo($product->product_type) ?? [];
        $autofills = AutoFill::all();

        $company = session('selectCompany') ?? "";

        return view('addproduct', ["company" => $company, 'isOfferte' => $isOfferte, 'isInclBTW' => $isIncBTW, 'product' => $product, 'productInfo' => $productInfo, 'autoFill' => $autofills]);
    }

    public function postEditProduct($id, addProduct $request)
    {
        $product = Product::findOrFail($id);

        $product->product_type = Input::get('selectProduct');
        $product->description = Input::get('message');
        $product->product_price = Input::get('price'); //product price...
        $product->count = Input::get('count');

        $alreadyAdded = AutoFill::where('text', $product->description)->first();
        if ($alreadyAdded == null) {
            $autoFill = new AutoFill();
            $autoFill->text = $product->description;
            $autoFill->save();
        }

        $pricePerUnit = 0;
        if (null !== Input::get('pricePreEenheid')) {
            $pricePerUnit = 1;
        }

        $product->pricePerUnit = $pricePerUnit;
        $product->unit = Input::get('eenheid');
        $product->productTotalPrice = Input::get('product_prijs'); // product total price ex btw
        $product->productTotalIncPrice = Input::get('product_total_inc_btw'); // product total inc btw
        $product->productBTW = Input::get('product_btw'); // total btw


        //accessories
        $accNames = Input::get('acc_name');
        $accPrices = Input::get('acc_price');
        $accCounts = Input::get('acc_count');
        $productAcc = array();
        for ($i = 0; $i < count($accNames); $i++) {
            //ignore empty values..
            if ($accNames[$i] == null || $accPrices[$i] == null || $accCounts[$i] == null)
                $productAcc[] = ['', 0, 0];
            else
                $productAcc[] = [$accNames[$i], $accPrices[$i], $accCounts[$i]];
        }
        $product->accessories = json_encode($productAcc);

        //photo
        if (Input::hasFile('photo')) {
            $product->photo = $this->upload();
        }

        //snijmaten
        $pnames = Input::get('snijmatenruimte');
        $lengths = Input::get('snijmatenwidth');
        $widths = Input::get('snijmatenlength');
        $cutMeasures = array();
        for ($i = 0; $i < count($pnames); $i++) {
            if ($pnames[$i] != null && $pnames[$i] != "" && $lengths[$i] != null && $lengths[$i] != "" && $widths[$i] != null && $widths[$i] != "")
                $cutMeasures[] = [$pnames[$i], $lengths[$i], $widths[$i]];
        }
        $product->cutMeasures = json_encode($cutMeasures);

        //plinten
        $product->plintenInfo = Input::get('plintenType');
        $product->plintCount = Input::get('plintenCount');
        $product->plintenPrice = Input::get('plintenPrice');
        $product->plintenDetails = Input::get('PlintenInfoDetails');


        //legservice
        $legService = 0;
        if (null !== Input::get('legserviceChecked')) {
            $legService = 1;
        }

        $product->legService = $legService;

        if ($legService == 1 && is_null($request['legservicePrice'])) {
            return redirect()->back()->withErrors(["legservicePrice" => "Legservice Prijs is verplicht als Legservice geselcteerd is"])->withInput();
        }
        $product->legServicePrice = isset($request['legservicePrice']) ? Input::get('legservicePrice') : 0;


        $products = session('products') ?? [];
        $isOfferte = session('isOfferte');

        //
        $company = session('selectCompany') ?? "";
        session(['selectCompany' => $company]);

        $product->save();

        $products[] = $product;
        session(['products' => $products]);

        session(['isOfferte' => $isOfferte]);


        $isIncBTW = session('isIncBTW');
        session(['isIncBTW' => $isIncBTW]);

        session()->save();

        return redirect()->route('productoverzicht', ['id' => $product->order_id]);
    }

    public function upload()
    {

        if (Input::hasFile('photo')) {


            $file = Input::file('photo');

            $filename = $file->getClientOriginalName();

            $file->move("uploads", $filename);

            return $filename;
        }
    }

    public function postAddProductError(addProduct $request)
    {
        return redirect()->back()->withErrors("Er is geen product geselecteerd!")->withInput();
    }

    public function copyProduct($id)
    {

        $product = Product::findOrFail($id);
        $newProduct = $product->replicate();
        $newProduct->push();
        return $newProduct->id;
    }

    public function postAddProduct($selectedProduct, addProduct $request)
    {

        $product = new Product();
        $product->product_type = $selectedProduct;
        $product->description = Input::get('message');
        $product->product_price = Input::get('price'); //product price...
        $product->count = Input::get('count');

        $alreadyAdded = AutoFill::where('text', $product->description)->first();

        if ($alreadyAdded == null) {
            $autoFill = new AutoFill();
            $autoFill->text = $product->description;
            $autoFill->save();
        }


        $pricePerUnit = 0;
        if (null !== Input::get('pricePreEenheid')) {
            $pricePerUnit = 1;
        }

        $product->pricePerUnit = $pricePerUnit;
        $product->unit = Input::get('eenheid');
        $product->productTotalPrice = Input::get('product_prijs'); // product total price ex btw
        $product->productTotalIncPrice = Input::get('product_total_inc_btw'); // product total inc btw
        $product->productBTW = Input::get('product_btw'); // total btw

        //accessories
        $accNames = Input::get('acc_name');
        $accPrices = Input::get('acc_price');
        $accCounts = Input::get('acc_count');
        $productAcc = array();
        for ($i = 0; $i < count($accNames); $i++) {
            //ignore empty values..
            if ($accNames[$i] == null || $accPrices[$i] == null || $accCounts[$i] == null)
                $productAcc[] = ['', 0, 0];
            else
                $productAcc[] = [$accNames[$i], $accPrices[$i], $accCounts[$i]];
        }
        $product->accessories = json_encode($productAcc);

        //photo
        if (Input::hasFile('photo')) {
            $product->photo = $this->upload();
        }

        //snijmaten
        $pnames = Input::get('snijmatenruimte');
        $lengths = Input::get('snijmatenwidth');
        $widths = Input::get('snijmatenlength');
        $cutMeasures = array();
        for ($i = 0; $i < count($pnames); $i++) {
            if ($pnames[$i] != null && $pnames[$i] != "" && $lengths[$i] != null && $lengths[$i] != "" && $widths[$i] != null && $widths[$i] != "")
                $cutMeasures[] = [$pnames[$i], $lengths[$i], $widths[$i]];
        }
        $product->cutMeasures = json_encode($cutMeasures);

        //plinten
        $product->plintenInfo = Input::get('plintenType');
        $product->plintCount = Input::get('plintenCount');
        $product->plintenPrice = Input::get('plintenPrice');
        $product->plintenDetails = Input::get('PlintenInfoDetails');


        //legservice
        $legService = 0;
        if (null !== Input::get('legserviceChecked')) {
            $legService = 1;
        }

        $product->legService = $legService;

        if ($legService == 1 && is_null($request['legservicePrice'])) {
            return redirect()->back()->withErrors(["legservicePrice" => "Legservice Prijs is verplicht als Legservice geselcteerd is"])->withInput();
        }
        $product->legServicePrice = isset($request['legservicePrice']) ? Input::get('legservicePrice') : 0;


        $products = session('products') ?? [];
        $isOfferte = session('isOfferte');

        //
        $company = session('selectCompany') ?? "";
        session(['selectCompany' => $company]);

        $order = session('order');
        if ($order == null) {
            $temp = new Order();
            $temp->isOfferte = $isOfferte;
            $temp->save();
            $order = $temp->id;
        }

        $product->order_id = $order;
        $product->save();
        $products[] = $product;
        session(['products' => $products]);

        session(['isOfferte' => $isOfferte]);


        $isIncBTW = session('isIncBTW');
        session(['isIncBTW' => $isIncBTW]);

        session()->save();

        return redirect()->route('productoverzicht');
    }
}
