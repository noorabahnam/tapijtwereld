<?php

namespace App\Http\Controllers;

use App\Http\Requests\addressRequest;
use App\Http\Requests\ProductOverview;
use App\Http\Requests\UserInfo;
use App\Order;
use Auth;
use Barryvdh\DomPDF\PDF;
use Dompdf\Dompdf;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\ImageManager;
use PDFA;
use function redirect;

class HomeController extends Controller
{
    private $products = array();
    private $order;
    private $company;
    private $employee;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        setlocale(LC_MONETARY, 'nl_NL');
    }

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function index()
    {
        $products = [];
//        $orders = new Order();
        session(['products' => $products]);
//        session(['order' => $orders]);


        return view('dashboard');
    }

    public function Logout()
    {
        Auth::logout();
        return redirect("/");
    }

    public function postindex(Request $request)
    {
        $request->validate([
            'selectCompany' => 'required',
            'selectedEmployee' => 'required',
        ]);

        $company = Input::get('selectCompany');
        $employee = Input::get('selectedEmployee') == "Flex" ? Input::get('name') : Input::get('selectedEmployee');
        $isOfferte = Input::get('isOfferte');
        $isIncBTW = Input::get('isIncBTW');

        //create the order here.
        $order = new Order();
        $order->isOfferte = $isOfferte;
        $order->inc_btw = $isIncBTW;
        $order->bedrijfsnaam = $company;
        $order->medewekernaam = $employee;
        $order->save();

        session(['order' => $order->id]);
        session(['selectCompany' => $company]);
        session(['selectedEmployee' => $employee]);
        session(['isOfferte' => $isOfferte]);
        session(['isIncBTW' => $isIncBTW]);

        return redirect()->route('AddProduct');
    }


    function setSessionItems($product)
    {
        $company = session('selectCompany') ?? "";
        session(['selectCompany' => $company]);

        $products[] = $product;
        session(['products' => $products]);

        $isOfferte = session('isOfferte');
        session(['isOfferte' => $isOfferte]);


        $isIncBTW = session('isIncBTW');
        session(['isIncBTW' => $isIncBTW]);

        session()->save();
    }


    public function ProductOverzicht($id = null)
    {
        $orderId = session('order');

        $order = Order::where('id', $orderId)->first();
        if ($id != null) {
            //todo order ophalen
            $order = Order::with('products')->find($id);

            session(['products' => $order->products]);
            session(['order' => $order->id]);
        }
//
        $company = session('selectCompany') ?? "";
        session(['selectCompany' => $company]);
        $products = session('products') ?? [];
        $isOfferte = session('isOfferte');
        session(['isOfferte' => $isOfferte]);


        $isIncBTW = session('isIncBTW');
        session(['isIncBTW' => $isIncBTW]);

        session()->save();

        return view('productoverzicht', ['order' => $order, 'products' => $products, 'company' => $company, 'isOfferte' => $isOfferte, 'isInclBTW' => $order->inc_btw]);
    }

    public function destroy($id)
    {   //For Deleting product
        $products = session('products') ?? [];

        unset($products[$id]);
        session(['products' => $products]);
        return response()->json([
            'success' => $products
        ]);
    }

    public function PostProductOverzicht(ProductOverview $request)
    {
        $products = session('products') ?? [];
        $order_id = session('order') ?? 0;
        $orders = Order::findOrFail($order_id);

        $orders->totaalprijs = Input::get('totaalprijs');// order total price ex btw

        $orders->order_total_inc_btw = Input::get('product_total_inc_btw'); // order total inc btw
        $orders->order_btw = Input::get('product_btw'); // total btw
        $dates = explode(" - ", Input::get('datepicker'));
        $orders->bezorgdatumBegin = $dates[0];
        $orders->bezorgdatumEnd = $dates[1];
        if (Input::get('optionsRadios') == 'option2' && is_null($request['datepicker'])) {
            return redirect()->back()->withErrors(["date" => "Bezorgdatum is verplicht als bezorgen geselcteerd is"])->withInput();
        }

        if (is_nan(floatval(Input::get('totaalprijs')))) {
            return redirect()->back()->withErrors(["date" => "Prijs is verkeerd!"])->withInput();
        }

        $orders->bezorgen = Input::get('optionsRadios');

        $dates = explode(" - ", Input::get('datepicker'));
        $orders->bezorgdatumBegin = $dates[0];
        $orders->bezorgdatumEnd = $dates[1];
        $orders->betaalmethode = Input::get('paymentType');
        $orders->korting = Input::get('korting') ?? 0;
        $orders->aanbetaling = Input::get('Aanbetaling') ?? 0;


//        dd($orders);
        $orders->save();
        session(['order' => $orders->id]);

        $company = session('selectCompany') ?? "";
        session(['selectCompany' => $company]);

        $isOfferte = session('isOfferte');
        session(['isOfferte' => $isOfferte]);

        $isIncBTW = session('isIncBTW');
        session(['isIncBTW' => $isIncBTW]);
        session()->save();

        return redirect()->route('adresgegevens');
    }

    public function BestelerGegevens()
    {

        $company = session('selectCompany') ?? "";
        session(['selectCompany' => $company]);
        $products = session('products') ?? [];
        $order_id = session('order') ?? 0;
        $order = Order::findOrFail($order_id);
        session(['order' => $order_id]);

        session(['products' => $products]);

        $isOfferte = session('isOfferte');
        session(['isOfferte' => $isOfferte]);

        $isIncBTW = session('isIncBTW');
        session(['isIncBTW' => $isIncBTW]);

        session()->save();
        return view('adresgegevens', ['order' => $order, 'company' => $company, 'isOfferte' => $isOfferte, 'isInclBTW' => $isIncBTW]);
    }

    public function postBestelerGegevens(Request $request)
    {

        $order_id = $request->get('order_id');

        $order = Order::findOrFail($order_id);
        $order->naam = Input::get('name');
        $order->email = $request->get('email') ?? "";
        $order->tel = Input::get('tel');
        $order->postcode = strtoupper(Input::get('postcode')) ?? "";
        $order->huisnummer = Input::get('hn');
        $order->straatnaam = Input::get('sn');
        $order->plaats = Input::get('city');
        $order->country = Input::get('country') ?? "NL";
        $order->postcodeaflever = strtoupper(Input::get('postcodeaflever')) ?? "";
        $order->huisnummeraflever = Input::get('hnaflever');
        $order->straatnaamaflever = Input::get('snaflever');
        $order->plaatsaflever = Input::get('cityaflever');
        $order->countryaflever = Input::get('countryaflever') ?? "NL";

        $input = $request->all();
        $validator = Validator::make($input, [
            'name' => 'required',
            'tel' => 'required',
            'hn' => 'required',
            'sn' => 'required',
            'city' => 'required',
            'postcode' => 'required|regex:/^(?:NL-)?(\d{4})\s*([A-Z]{2})$/i',
            'postcodeaflever' => 'nullable|regex:/^(?:NL-)?(\d{4})\s*([A-Z]{2})$/i'
        ]);


        if ($validator->fails()) {
            session(['old' => $input]);
            session()->save();
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }
        $products = session('products') ?? [];

        session(['products' => $products]);
        session(['order' => $order->id]);
        $company = session('selectCompany') ?? "";
        session(['selectCompany' => $company]);


        $isIncBTW = session('isIncBTW');
        session(['isIncBTW' => $isIncBTW]);
        session()->save();

        $order->save();
        return redirect()->route('FinalOverzicht');
    }

    Public function FinalOverzicht()
    {
        $isOfferte = session("isOfferte");
        session(['isOfferte' => $isOfferte]);

        $company = session('selectCompany') ?? "";
        session(['selectCompany' => $company]);

        $employee = session('selectedEmployee') ?? "";
        session(['selectedEmployee' => $employee]);


        $isIncBTW = session('isIncBTW');
        session(['isIncBTW' => $isIncBTW]);

        $order_id = session('order');
        $order = Order::findOrFail($order_id);

        return view('finaloverzicht', ['order' => $order, 'products' => $order->products, 'company' => $company, 'employee' => $employee, 'isOfferte' => $isOfferte, 'isInclBTW' => $order->inc_btw]);
    }

    Public function postFinalOverzicht(Request $request)
    {
        $products = session('products') ?? [];
        session(['products' => $products]);

        $order_id = session('order') ?? 0;
        $orders = Order::findOrFail($order_id);

        $company = session('selectCompany') ?? "";
        session(['selectCompany' => $company]);


        $isIncBTW = session('isIncBTW');
        session(['isIncBTW' => $isIncBTW]);
        session()->save();

        $isOfferte = session("isOfferte") ?? 0;
        if ($isOfferte == 1) {
            $orders->finished = 1;
            $isOfferte = Input::get('offerte') != null ? 0 : 1;
            session(['isOfferte' => $isOfferte]);
            session()->save();
        }

        return redirect()->route('ondertekenen');
    }


    public function Ondertekenen()
    {
        $isOfferte = session("isOfferte") ?? 0;
        $company = session('selectCompany') ?? "";
        session(['selectCompany' => $company]);

        $order_id = session('order') ?? 0;
        $order = Order::findOrFail($order_id);

        $products = session('products') ?? [];
        session(['products' => $products]);
        session(['order' => $order->id]);
        session()->save();

        if ($isOfferte == true) {
            return redirect(route('pdf'));
        }

        return view('ondertekenen', ['products' => $products, 'order' => $order, 'company' => $company, 'isOfferte' => $isOfferte]);
    }

    public function postOndertekenen(Request $request)
    {
        $products = session('products') ?? [];
        $order_id = session('order') ?? 0;
        $order = Order::findOrFail($order_id);


        $request->validate([
            'signature' => 'required',
            'terms' => 'required'
        ]);

        $order->handtekenen = Input::get('signature');
        session(['products' => $products]);
        session(['order' => $order->id]);


        $isIncBTW = session('isIncBTW');
        session(['isIncBTW' => $isIncBTW]);

        session()->save();

        $file = $order["handtekenen"];

        $filename = "order_" . $order->id . ".png";
        $path = public_path() . "/uploads/" . $filename;
        (new ImageManager())->make(file_get_contents($file))->save($path);
        $order->handtekenen = $filename;
        $order->finished = 1;
        $order->save();

        $company = session('selectCompany') ?? "";
        session(['selectCompany' => $company]);


        return redirect()->route('pdf');
    }

    public function PDFOverzicht()
    {
        $isOfferte = session("isOfferte") ?? 0;
        session(['isOfferte' => $isOfferte]);
        $company = session('selectCompany') ?? "";
        session(['selectCompany' => $company]);
        $employee = session('selectedEmployee') ?? "";
        session(['selectedEmployee' => $employee]);

        $order_id = session('order') ?? 0;
        $order = Order::findOrFail($order_id);
        return view('pdfoverzicht', ['order' => $order, 'products' => $order->products, 'company' => $company, 'employee' => $employee, 'isPDF' => false, 'isOfferte' => $isOfferte, 'isInclBTW' => $order->inc_btw]);

    }

    public function downloadPDF(Request $request)
    {

        $isOfferte = session("isOfferte") ?? 0;
        session(['isOfferte' => $isOfferte]);
        $company = session('selectCompany') ?? "";
        session(['selectCompany' => $company]);
        $employee = session('selectedEmployee') ?? "";
        session(['selectedEmployee' => $employee]);
        $order_id = session('order') ?? 0;

        $orders = Order::with('products')->findOrFail($order_id);

        if ($orders->isOfferte == true) {
            $pdf = PDF::loadView('factuur', ['products' => $orders->products, 'order' => $orders, 'company' => $orders->bedrijfsnaam, 'employee' => $orders->medewekernaam, 'isPDF' => true, 'isOfferte' => $orders->isOfferte, 'isInclBTW' => $orders->inc_btw])->setPaper('a4', 'portrait')->setWarnings(true);
        } else {
            $pdf = PDF::loadView('factuur', ['products' => $orders->products, 'order' => $orders, 'company' => $orders->bedrijfsnaam, 'employee' => $orders->medewekernaam, 'isPDF' => true, 'isOfferte' => $orders->isOfferte, 'isInclBTW' => $orders->inc_btw])->setPaper('a4', 'portrait')->setWarnings(true);
        }

        $pdfName = $orders->isOfferte ? "Offerte-" : "Bestelling-" . $orders->id . "-" . $orders->naam . "-" . $orders->bedrijfsnaam . ".pdf";
        if (file_exists(storage_path($pdfName)))
            unlink(storage_path($pdfName));
        $pdf->save(storage_path($pdfName));


        $user = $orders->email;
        Mail::send('emails.order', ['products' => $orders->products, 'order' => $orders, 'company' => $orders->bedrijfsnaam, 'employee' => $orders->medewekernaam, 'isPDF' => true, 'isOfferte' => $orders->isOfferte, 'isInclBTW' => $orders->inc_btw], function ($m) use ($pdfName, $user) {
            $m->to($user, $user)->subject($pdfName);
            $m->attach(storage_path($pdfName));
        });


        return $pdf->download($pdfName);


    }

    public function downloadPDFForOrder($id)
    {
        $orders = Order::with('products')->where('id', $id)->first();

        if ($orders->isOfferte == true) {
            $pdf = PDFA::loadView('factuur', ['products' => $orders->products, 'order' => $orders, 'company' => $orders->bedrijfsnaam, 'employee' => $orders->medewekernaam, 'isPDF' => true, 'isOfferte' => $orders->isOfferte, 'isInclBTW' => $orders->inc_btw])->setPaper('a4', 'portrait')->setWarnings(true)->setOption('toc', true);
        } else {
            $pdf = PDFA::loadView('factuur', ['products' => $orders->products, 'order' => $orders, 'company' => $orders->bedrijfsnaam, 'employee' => $orders->medewekernaam, 'isPDF' => true, 'isOfferte' => $orders->isOfferte, 'isInclBTW' => $orders->inc_btw])->setPaper('a4', 'portrait')->setWarnings(true);
        }

        $pdfName = $orders->isOfferte ? "Offerte-" : "Bestelling-" . $orders->id . "-" . $orders->naam . "-" . $orders->bedrijfsnaam . ".pdf";
        if (file_exists(storage_path($pdfName)))
            unlink(storage_path($pdfName));
        $pdf->save(storage_path($pdfName));


        $user = $orders->email;
        Mail::send('emails.order', ['products' => $orders->products, 'order' => $orders, 'company' => $orders->bedrijfsnaam, 'employee' => $orders->medewekernaam, 'isPDF' => true, 'isOfferte' => $orders->isOfferte, 'isInclBTW' => $orders->inc_btw], function ($m) use ($pdfName, $user) {
            $m->to($user, $user)->subject($pdfName);
            $m->attach(storage_path($pdfName));
        });

        return $pdf->download($pdfName);
    }

    public function Overzichtspagina()
    {
        $order = Order::where('finished', 1)->orderBy('created_at', 'desc')->get();

        return view('overzichtspagina', ['orders' => $order]);
    }

    public function postOverzichtspagina(Request $request)
    {
        if (strcmp($request["status_type"], "SelectedStatus") == 0) {
            return redirect()->back()->withErrors(["status_type" => "Selecteer Status type"])->withInput();
        }

        if (strcmp($request["payment_type"], "PaymentStatus") == 0) {
            return redirect()->back()->withErrors(["payment_type" => "Selecteer betaling status"])->withInput();
        }
        $order_id = $request->get('order_id');
        $order = Order::findOrFail($order_id);

        $order->status_type = Input::get('status_type');
        $order->payment_type = Input::get('payment_type');

        $order->save();
    }


    public function BestellingWijzigen()
    {
        $company = session('selectCompany') ?? "";
        session(['selectCompany' => $company]);
        return view('bestellingzoeken', ['company' => $company]);
    }

    public function postBestellingWijzigen(Request $request)
    {
        $orders = [];
        $postcode = $request["postcode"];
        $huisnummer = $request["huisnummer"];
        $orderId = $request["order_id"];

        if ($orderId != null) {
            $order = Order::where('id', $orderId)->orWhere('naam', $orderId)->with('products')->first();
            if ($order != null) $orders[] = $order;
        } else {

            if ($huisnummer != null && $postcode != null) {
                $orders = Order::where('postcode', $postcode)->orWhere('huisnummer', $huisnummer)->with('products')->get();
            } else if ($postcode != null) {
                $orders = Order::where('postcode', $postcode)->with('products')->get();
            } else if ($huisnummer != null) {
                $orders = Order::where('huisnummer', $huisnummer)->with('products')->get();
            }
        }

        $company = session('selectCompany') ?? "";
        session(['selectCompany' => $company]);
        session(['orders' => $orders]);


        $isIncBTW = session('isIncBTW');
        session(['isIncBTW' => $isIncBTW]);

        session()->save();

        return redirect()->route('zoeken');
    }

    public function Zoekresultaten()
    {
        $orders = session('orders') ?? [];

        return view('zoekresultaten', ['orders' => $orders]);

    }

    public function postZoekresultaten(Request $request)
    {
        return redirect()->route('zoeken');

    }


}
